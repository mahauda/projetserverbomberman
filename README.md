# Projet Serveur Bomberman

Réalisé par :
- Théo MAHAUDA
- Anas TAGUENITI
- Mohamed OUHIRRA

Date : 09/04/2020\
Organisation : Master Informatique M1 à l'université d'Angers\
Version : 1.0\
Référence projet GitLab : https://gitlab.com/mahauda/projetserverbomberman

## Objectif

Permettre de gérer des parties du jeu Bomberman afin de controller les règles du jeu. C'est le serveur qui décide / valide ce qui passe dans une partie.

## Architecture

<img src="src/main/resources/images/Architecture.png"/>

### Requetes / Notifications

Il y a deux type de communications entre le serveur et le client, soit on communique un traitement comme par exemple faire bouger l'agent bomberman, et dans ce cas on utilise des requetes qui envoit une demande au serveur afin de recevoir un traitement particulier,
soit on communique des informations par exemple quand un joueur gagne la partie, et dans ce cas on utilise les notifications qui envoient une demande au serveur sans pour autant recevoir une réponse.
Afin de créer une requete ou bien une notification, il faut créer une classe qui implemente l'interface Commandable du coté du client et du serveur, puis l'enregistrer au niveau du "ServiceClient" et "ServiceServeur".

### Communication avec le client

À travers des sockets, le client et le serveur communique en JSON, les données sont convertis en JSON à l'envois et reconvertis en Object à la réception en utilisant une classe Player qui contient tous les attributs dont on a besoin pour transmettre non seulement les données du jeu mais aussi les messages à transmettre à chaque utilisateur.
Les requetes envoyées par le client suivent un mecanisme de verrou, cela veut dire qu'un client ne peut envoyer qu'une requete à la fois afin de synchroniser les échanges avec le serveur.

## Fonctionnalités implémentées

### Connexion d'un joueur

Apres que la reception du login et mot de passe de la part du client, et en utilisant la classe "ServerRequestLogin", le serveur d'abord interroge l'API pour vérifier bien que le login (Pseudo) existe dèja au niveau de la base de données, puis il verifie le mot de passe, si tout s'est bien passé, il crée le joueur et l'enregistre, ensuite il envoit la reponse au client.

<img src="src/main/resources/images/Login.png"/>

### Déconnexion d'un joueur

Comme dans la connexion, le serveur recoit une demande de deconnexion de la part du client, et dans la classe "ServerRequestLogout" on envoit d'abord un message au joueur qui se deconnecte, puis on fait une verification au niveau du tour de jeu, si c'était son tour de jouer, on passe la main à un autre joueur dans le cas du multi-joueur et on attend 1 minute avant de supprimer le joueur afin qu'il puisse recevoir le message de deconnexion. 

### Gestion des parties multi-joueur

Principes multi-joueurs:
- Chaque joueur joue son tour  dans la partie dans l'ordre de son arrivé, par exemple si Momo arrive en 1er, puis Anas en second et enfin Théo en dernier, alors c'est d'abord Momo qui joue, puis Anas et enfin Théo et on revient à Momo, etc. jusqu'a la fin de la partie. Si un joueur se deconnecte durant la partie et que c'était son tour de jouer, on passe la main au joueur qui est venu juste après
- Chaque joueur à le droit d'effectuer qu'un seul coup (soit déplacé le bomberman, soit posé une bombe) et après ce coup la main est passé à un autre joueur.
Après ce coup la main est passé à l'autre joueur.
- Chaque joueur peut entrer dans la partie à n'importe quelle moment (au début, en plein milieu ou vers la fin).

#### Poser une bombe

Pour les actions effectuées par le joueur que ce soit poser une bombe ou deplacer le bomberman, nous avons implementés une classe générique "ServerRequestAction" qui implemente "Commandable<Player>", qui contient des methodes permettant de :
- Verifier si c'est au joueur courant de jouer.
- Recuperer le prochain joueur qui doit jouer.
- Passer la main au prochain joueur.
La classe "ServerRequestActionPutBomb" hérite donc de "ServerRequestAction", et après avoir fait les verifcation necessaires coté serveur, on met a jour le nouvel etat des bombes et on renvoit la reponse au Client.

<img src="src/main/resources/images/PoserBombe.png"/>

#### Déplacer le Bomberman

Comme pour "ServerRequestActionPutBomb", la classe "ServerRequestMoveBomberman" fait les memes verifications et met a jour le deplacement du bomberman coté serveur et envois le nouveau traitement au client afin qu'il puisse mettre à jour le nouvel état du jeu.

<img src="src/main/resources/images/Mouvement.png"/>

### Mise à jour du jeu à envoyé pour les clients

La classe "ServerNotifyTurn" permet d'informer à la vue coté client les nouvelles informations du jeu à chaque tour, en se basant sur les notifications présentées ci-dessous

#### L'état de la partie

L'état de la partie se met a jour grace la classe "ServerNotifyGame", cette classe qui implemente aussi l'interface Commandable permet d'informer à la vue coté client si le jeu continue toujours. Si le joueur remporte la partie, on recupere la communication et la socket service, et met a jour l'objet Player qui permet de regrouper toutes les informations à envoyer au client, ensuite on lance une nouvelle partie, puis on recupere l'utilisateur du serveur grace à l'identifiant (port,ip), puis on recupere le compte associé à l'utilisateur via l'API, et finalement on crée une nouvelle partie à partir des informations dans le serveur (Score,niveau,...) et on l'ajoute à la liste des parties selon le resultat (Gagné ou perdu), puis on met à jout le compte du joueur via l'API.

#### Le déplacement du Bomberman

Après chaque deplacement de l'agent Bomberman, le serveur envoit une notification "ServerNotifyMoveBomberman" avec les nouvelles coordonnées de l'agent.

#### Le déplacement des ennemis

Après chaque tour, les agents ennemis changent de position, le serveur envoit une notification "ServerNotifyMoveEnnemy" avec les nouvelles coordonnées des agents ennemis.

#### Les bombes posées

Quand un joueur pose une bombe, une notification "ServerNotifyActionPutBomb" est envoyée au client afin d'informer à la vue coté client les nouvelles bombes posées par le bomberman.

#### Les bonus appliqués sur le Bomberman
 
Quand un joueur explose un mur, il a le droit à un bonus, la classe "ServerNotifyApplyBonus" permet de mettre ajour les bonus du joueur après l'application de ces derniers par le serveur et informe la vue coté client les bonus appliqués au bomberman.

#### Les impacts d'attaques des ennemis sur le Bomberman

Les agent ennemis peuvent attaquer l'agent bomberman depandement de leur nature, donc pour mettre à jour le nouvel état du jeu après une attaque, le serveur envois une notification "ServerNotifyAttackBomberman" qui contient le nouvel etat du bomberman après une attaque pour informer la vue coté client.

### Fin de partie 
#### Perdue
Quand un joueur se fait éliminer par un ennemi après un tour, la classe "ServerNotifyGame" communique au client le résultat de la partie et une fenêtre pop-up s'affiche comme ci-dessous.
<img src="src/main/resources/images/Perdu.png"/>
#### Gagné
Quand un joueur gagne une partie, le serveur communique cette information grace à la classe "ServerNotifyGame" en y ajoutant le prochain niveau si le joueur souhaite continuer à jouer.
<img src="src/main/resources/images/Gagne.png"/> 
## Fonctionnalités non implémenté
- Comme vous l'avez remarqué, les joueurs ne peuvent manipuler qu'un seul Bomberman, en effet nous respectons le principe du multijoueur mais il reste à implementer une nouvelle fonctionnalité qui permettra aux joueurs de jouer un contre un, ou équipe contre équipe.
- Avec l'implementation qu'on a réalisé, on pourrait rajouter le principe d'équipe dans le jeu, ainsi les utilisateurs peuvent créer des équipes ou des clans et s'affronter en équipe, on pourrait bien imaginer un classement particulier pour les équipes ainsi que des récompenses d'équipes etc .