package universite.angers.master.info.server.bomberman.controller.itemable.bonus.life;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Invincibilité (ou encore Fire Suit, FlameProof Jacket) - Permet au bomberman d'être totalement invincible pendant x tours. 
 * L'utilisateur de ce bonus clignote rapidement.
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusFireSuit extends AgentItemableBonusLife {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusFireSuit() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		return itemType == ItemType.FIRE_SUIT;
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		bomberman.setInvincible(true);
	}
}
