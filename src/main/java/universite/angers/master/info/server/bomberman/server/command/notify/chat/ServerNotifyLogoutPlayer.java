package universite.angers.master.info.server.bomberman.server.command.notify.chat;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet d'informer les joueurs d'une déconnexion d'un joueur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyLogoutPlayer implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyLogoutPlayer.class);

	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify Logout player");
		Player playermessage = new Player();

		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if (arg == null) {
			player = new Player();
		} else {
			player = (Player) arg;
		}
		
		// On indique aux autres joueur qu'un joueur a quitté la partie
		playermessage.getMessages().add("[Serveur] : Le joueur " + player.getLogin() + " s'est déconnecté");
		LOG.debug("Le joueur " + player.getLogin() + " s'est déconnecté");

		playermessage.setCommand("NOTIFY_LOGOUT_PLAYER");
		LOG.debug("Player : " + playermessage);
		
		return playermessage;
	}
}
