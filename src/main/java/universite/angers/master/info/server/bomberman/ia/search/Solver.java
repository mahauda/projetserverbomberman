package universite.angers.master.info.server.bomberman.ia.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import universite.angers.master.info.server.bomberman.ia.search.explorable.Explorable;
import universite.angers.master.info.server.bomberman.ia.search.explorable.ExplorationA;
import universite.angers.master.info.server.bomberman.ia.search.explorable.ExplorationLargeur;
import universite.angers.master.info.server.bomberman.ia.search.explorable.ExplorationProfondeur;
import universite.angers.master.info.server.bomberman.ia.solve.FactoryAction;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.Heuristic;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.HeuristicBombermanEuclidienne;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.HeuristicBombermanManhattan;
import universite.angers.master.info.server.bomberman.model.bomberman.map.MapAdapter;
import universite.angers.master.info.server.bomberman.util.FileUtil;

/**
 * Simulation de test du jeu avec les 3 niveaux selon des stratégies optées Ces
 * simulations sont écrits dans des fichiers
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Solver {

	/**
	 * Test
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// Paramètre non pris en compte dans la résolution
		int maxTurn = 100;

		// Paramètre non pris en compte dans la résolution
		long time = 1000;

		// Les niveaux des labyrinthes
		String pathFileLabLevel1 = "./layouts/niveau1.lay";
		String pathFileLabLevel2 = "./layouts/niveau2.lay";
		String pathFileLabLevel3 = "./layouts/niveau3.lay";
		List<String> pathFileLabAllLevel = Arrays.asList(pathFileLabLevel1, pathFileLabLevel2, pathFileLabLevel3);

		// Les actions possibles
		// 1 = haut
		// 2 = bas
		// 3 = gauche
		// 4 = droite
		// 5 = poser une bombe

		// Création des 120 combinaisons
		int[] actions = new int[] { 1, 2, 3, 4, 5 };
		FactoryAction.generateAllCombinaisons(actions, 0);
		List<List<Integer>> combinaisonsActions = FactoryAction.getCombinaisons();

		// Création des 3 modes d'exploration
		List<Explorable<MapAdapter>> frontiers = new ArrayList<>(
				Arrays.asList(new ExplorationProfondeur<>(), new ExplorationLargeur<>(), new ExplorationA<>()));

		// Création des deux heuristiques
		List<Heuristic<MapAdapter>> heuristics = new ArrayList<>(
				Arrays.asList(new HeuristicBombermanManhattan(), new HeuristicBombermanEuclidienne()));

		for (String pathFileLabLevel : pathFileLabAllLevel) {
			for (Explorable<MapAdapter> frontier : frontiers) {
				for (Heuristic<MapAdapter> heuristic : heuristics) {
					for (List<Integer> combinaisonActions : combinaisonsActions) {
						// Problem java.util.ConcurrentModificationException --> obliger de réinstancier
						if (frontier instanceof ExplorationProfondeur)
							frontier = new ExplorationProfondeur<>();
						if (frontier instanceof ExplorationLargeur)
							frontier = new ExplorationLargeur<>();
						if (frontier instanceof ExplorationA)
							frontier = new ExplorationA<>();

						long start = System.currentTimeMillis();

						Node<MapAdapter> soluce = FactorySearch.getSoluceBomberman(maxTurn, time, pathFileLabLevel,
								combinaisonActions, frontier, heuristic);

						long end = System.currentTimeMillis();

						String levelName = pathFileLabLevel.substring(pathFileLabLevel.length() - 11,
								pathFileLabLevel.length() - 4);
						String frontierName = frontier.getNom();
						String heuristicName = heuristic.getName();
						String combinaisonName = combinaisonActions.stream().map(String::valueOf)
								.collect(Collectors.joining("_"));
						String path = String.format("./soluces/%s_%s_%s_%s.txt", levelName, frontierName, heuristicName,
								combinaisonName);

						// On écrit la solution dans un fichier
						FileUtil.writeFile("", path, false);
						FileUtil.writeFile("Level : " + levelName + "\n", path, true);
						FileUtil.writeFile("Frontier : " + frontierName + "\n", path, true);
						FileUtil.writeFile("Heuristic : " + heuristicName + "\n", path, true);
						FileUtil.writeFile("Combinaison : " + combinaisonActions.toString() + "\n", path, true);
						FileUtil.writeFile("Total nodes create : " + Node.getTotalNodeCreate() + "\n", path, true);
						FileUtil.writeFile("Time : " + String.valueOf(end - start) + " ms\n", path, true);
						FileUtil.writeFile("\nStep to solve : \n" + soluce, path, true);
					}
				}
			}
		}
	}
}
