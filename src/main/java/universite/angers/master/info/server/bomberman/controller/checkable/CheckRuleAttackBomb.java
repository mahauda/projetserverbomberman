package universite.angers.master.info.server.bomberman.controller.checkable;

import java.util.Iterator;
import java.util.Random;
import universite.angers.master.info.server.bomberman.controller.actionnable.InfoBomb;
import universite.angers.master.info.server.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.server.bomberman.controller.itemable.InfoItem;
import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Vérifier l'état des bombes posées
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CheckRuleAttackBomb extends CheckRule {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean checkRule() {
		return !Map.getInstance().getStart_Bombs().isEmpty();
	}
	
	@Override
	public void executeRule() {
		for(Iterator<InfoBomb> iteratorBomb = Map.getInstance().getStart_Bombs().iterator(); iteratorBomb.hasNext(); ) {
			InfoBomb bomb = iteratorBomb.next();
			switch(bomb.getStateBomb()) {
				case Step1:
					bomb.setStateBomb(StateBomb.Step2);
					break;
				case Step2:
					bomb.setStateBomb(StateBomb.Step3);
					break;
				case Step3:
					bomb.setStateBomb(StateBomb.Boom);
					break;
				case Boom:
					this.checkImpactBomb(bomb);
					iteratorBomb.remove();
					break;
			}
		}
		
		//ServerBombermanGame.getInstance().getServer().notifyAllObservers("NOTIFY_ATTACK_BOMB", null)
	}
	
	/**
	 * Vérifie l'impact de la bombe sur l'environnement
	 * @param bomb
	 */
	private void checkImpactBomb(InfoBomb bomb) {
		int points = 0;
		
		for (int i=0; i<=bomb.getRange(); i++) {

			//Vers le haut
			if(bomb.getY()-i >= 0) {
				points += this.breakWall(bomb.getX(), bomb.getY()-i);
				points += this.killEnemy(bomb.getX(), bomb.getY()-i);
			}
			
			//Vers le bas
			if(bomb.getY()+i < Map.getInstance().getSize_y()) {	
				points += this.breakWall(bomb.getX(), bomb.getY()+i);
				points += this.killEnemy(bomb.getX(), bomb.getY()+i);
			}
			
			//Vers la droite
			if(bomb.getX()+i < Map.getInstance().getSize_x()) {	
				points += this.breakWall(bomb.getX()+i, bomb.getY());
				points += this.killEnemy(bomb.getX()+i, bomb.getY());
			}
			
			//Vers la gauche
			if(bomb.getX()-i >= 0) { 
				points += this.breakWall(bomb.getX()-i, bomb.getY());
				points += this.killEnemy(bomb.getX()-i, bomb.getY());
			}			
		}
		
		//Incrémente des points au bomberman
		CapacityItem point = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.POINT);
		if(point == null) return;
		
		int actualPoint = point.getCapacityActualItem();
		point.setCapacityActualItem(actualPoint + points);
	}
	
	/**
	 * On élimine les ennemis qui sont dans le périmétre de la bombe
	 */
	private int killEnemy(int bombX, int bombY) {
		int points = 0;
		//Pour chaque ennemi
		for(Iterator<AgentEnnemy> iteratorAgent = Map.getInstance().getAgentsEnnemy().iterator(); iteratorAgent.hasNext(); ) {
			AgentEnnemy agent = iteratorAgent.next();
			
			//On récupère les coordonnées de l'agent
			int agentX = agent.getX();
			int agentY = agent.getY();
			
			//Si l'agent est dans la zone d'impact de la bombe
			if(agentX == bombX && agentY == bombY) {
				iteratorAgent.remove();
				Map.getInstance().getAgents().remove(agent);
				Map.getInstance().getStart_Agents().remove(agent);
				points += 50;
			}
		}
		return points;
	}
	
	/**
	 * On détruit les murs cassable et on place un bonus aléatoire
	 * @param bomb
	 */
	private int breakWall(int bombX, int bombY) {
		if(Map.getInstance().getStart_brokable_walls()[bombX][bombY]) {
			Map.getInstance().getStart_brokable_walls()[bombX][bombY] = false;
			//On y place un bonus aléatoire
			this.addBonus(bombX, bombY);
			return 10;
		} else return 0;	
	}
	
	/**
	 * Choisir un bonus aléatoirement
	 * @return le bonus tirée aléatoirement
	 */
	private ItemType choseRandomBonus() {
		int maxBonus = ItemType.valuesUse().size();
		int randomBonus = new Random().nextInt(maxBonus);
		
		return ItemType.valuesUse().get(randomBonus);
	}
	
	/**
	 * Ajouter un bonus aléatoire dans la map
	 * @param x
	 * @param y
	 */
	private void addBonus(int x, int y) {
		ItemType bonus = this.choseRandomBonus();
		InfoItem item = new InfoItem(x, y, bonus);
		Map.getInstance().getStart_Items().add(item);	
	}
}
