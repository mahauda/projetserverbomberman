package universite.angers.master.info.server.bomberman.server.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.controller.actionnable.AgentActionnablePutBomb;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Classe qui permet d'envoyer une requete au serveur pour poser une bombe
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestActionPutBomb extends ServerRequestAction {

	private static final Logger LOG = Logger.getLogger(ServerRequestActionPutBomb.class);

	private Player player;

	@Override
	public boolean send(Player player) {

		LOG.debug("Request put bomb player : " + player);

		this.player = new Player();
		
		boolean isTurn = this.isPlayerTurn(player);
		LOG.debug("Is turn : " + isTurn);
		
		//Si ce n'est pas son tour dans ce cas
		if (!isTurn) {
			this.player.getMessages().add("Ce n'est pas à vous de jouer ! ");
			return true;
		}

		AgentAction action = player.getAction();
		LOG.debug("Request put bomb action : " + action);

		AgentActionnablePutBomb putBomb = new AgentActionnablePutBomb();
		boolean record = putBomb.isLegalAction(Map.getInstance().getCurrentAgentKind(), action);
		LOG.debug("Request put bomb record : " + record);

		//Si le coup est validé alors
		if (record) {
			//On effectue l'action
			putBomb.doAction(Map.getInstance().getCurrentAgentKind(), action);
			
			//On passe la main à un autre joueur
			this.passTurnToOtherPlayer(player);
		}

		//On enregistre les nouvelles bombes ajoutés afin de les afficher coté clients
		this.player.setBombs(Map.getInstance().getStart_Bombs());
		this.player.setAction(action);
		this.player.setRecord(record);

		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_ACTION_PUT_BOMB");
		return this.player;
	}
}
