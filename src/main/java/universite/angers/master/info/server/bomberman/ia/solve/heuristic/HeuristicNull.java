package universite.angers.master.info.server.bomberman.ia.solve.heuristic;

import universite.angers.master.info.server.bomberman.ia.search.Node;
import universite.angers.master.info.server.bomberman.ia.solve.Solveable;

/**
 * Heuristique null pour les explorations qui n'utilisent pas d'heuristique
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class HeuristicNull<T extends Solveable> extends Heuristic<T> {

	public HeuristicNull() {

	}

	@Override
	public double calculHHeuristic(Node<T> n) {
		return 0;
	}

	@Override
	public double calculGHeuristic(Node<T> n) {
		return 0;
	}

	@Override
	public String toString() {
		return "HeuristicNull";
	}
}
