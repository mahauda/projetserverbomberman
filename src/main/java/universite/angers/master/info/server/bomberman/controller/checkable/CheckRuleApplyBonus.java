package universite.angers.master.info.server.bomberman.controller.checkable;

import java.util.Iterator;
import universite.angers.master.info.server.bomberman.controller.itemable.InfoItem;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Vérifier si un agent bomberman à récupérer un bonus. Si oui on l'applique
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CheckRuleApplyBonus extends CheckRule {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean checkRule() {
		return !Map.getInstance().getStart_Items().isEmpty();
	}

	@Override
	public void executeRule() {
		// Pour chaque bonus placé sur le plateau
		for (Iterator<InfoItem> iteratorItem = Map.getInstance().getStart_Items().iterator(); iteratorItem.hasNext();) {

			InfoItem bonus = iteratorItem.next();

			// On récupère les coordonnées du bonus
			int bonusX = bonus.getX();
			int bonusY = bonus.getY();

			// On récupère les coordonnées du bomberman
			int bombermanX = Map.getInstance().getCurrentAgentKind().getX();
			int bombermanY = Map.getInstance().getCurrentAgentKind().getY();

			// Si le bomberman est sur un bonus
			if (bonusX == bombermanX && bonusY == bombermanY) {

				// On applique le bonus sur le bomberman
				Map.getInstance().getCurrentAgentKind().doBonus(bonus.getType());

				// Puis on supprime le bonus du plateau
				iteratorItem.remove();
			}
		}
		
		//ServerBombermanGame.getInstance().getServer().notifyAllObservers("NOTIFY_APPLY_BONUS", null)
	}
}
