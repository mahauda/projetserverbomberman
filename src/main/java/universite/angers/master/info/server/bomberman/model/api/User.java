package universite.angers.master.info.server.bomberman.model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * Représente l'utilisateur avec ses infos générales, son classement (rang) dans le jeu et ses items débloquer
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Son identifiant unique généré automatiquement par le constructeur avec l'aide d'un UUID
	 */
	private String id;
	
	/**
	 * Son prénom
	 */
	private String firstName;
	
	/**
	 * Son nom
	 */
	private String lastName;
	
	/**
	 * L'adresse mail
	 */
	private String email;
	
	/**
	 * Le pseudo
	 */
	private String pseudo;
	
	/**
	 * Le mot de passe pour se connecter au site
	 */
	private String password;
	
	/**
	 * Son pays d'origine (utilisé dans le classement notamment)
	 */
	private String country;
	
	/**
	 * Sa date de naissance
	 */
	private Date birthday;
	
	/**
	 * Le classement du joueur dans le jeu
	 */
	private Rank rank;
	
	/**
	 * Les identifiant des items débloquer et acquis depuis la boutique
	 */
	private Collection<String> items;

	/**
	 * Constructeur par défaut sans argument pour l'ORM
	 */
	public User() {
		this("", "", "", "", "", "", new Date(), Rank.UNRANKED);
	}
	
	/**
	 * Constructeur avec arguments
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param pseudo
	 * @param password
	 * @param country
	 * @param birthday
	 */
	public User(String firstName, String lastName, String email, String pseudo, String password,
			String country, Date birthday, Rank rank) {
		this.id = UUID.randomUUID().toString();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.pseudo = pseudo;
		this.password = password;
		this.country = country;
		this.birthday = birthday;
		this.rank = rank;
		this.items = new ArrayList<>();
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * updateUser
	 */
	public  boolean updateUser(User user) {
		this.firstName = user.firstName;
		this.lastName = user.lastName;
		this.birthday = user.birthday;
		this.country = user.country;
		this.pseudo = user.pseudo;
		this.email = user.email;
		return true;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * @param pseudo the pseudo to set
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the rank
	 */
	public Rank getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(Rank rank) {
		this.rank = rank;
	}

	/**
	 * @return the items
	 */
	public Collection<String> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Collection<String> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		if (rank != other.rank)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", pseudo=" + pseudo + ", password=" + password + ", country=" + country + ", birthday=" + birthday
				+ ", rank=" + rank + ", items=" + items + "]";
	}
}

	