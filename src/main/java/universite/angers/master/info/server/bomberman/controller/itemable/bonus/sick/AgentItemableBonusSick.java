package universite.angers.master.info.server.bomberman.controller.itemable.bonus.sick;

import java.io.Serializable;
import universite.angers.master.info.server.bomberman.controller.itemable.Itemable;

/**
 * Bonus infections
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class AgentItemableBonusSick implements Itemable, Serializable {
	
	private static final long serialVersionUID = 1L;

	public AgentItemableBonusSick() {

	}
}
