package universite.angers.master.info.server.bomberman.controller;

import universite.angers.master.info.server.bomberman.ia.solve.Solveable;
import universite.angers.master.info.server.bomberman.model.Game;

/**
 * Interface qui permet d'assurer les contrôles du jeu quand des actions ont été effectuées par
 * l’utilisateur dans la Vue (par exemple lorsque l’utilisateur clique sur un bouton)
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 *
 */
public abstract class Controller<T extends Solveable> {
	
	/**
	 * Le jeu à controller
	 */
	protected Game<T> game;
	
	public Controller(Game<T> game) {
		this.game = game;
	}
	
	/**
	 * Méthode qui initialise la vue du jeu
	 */
	public abstract void init();
	
	/**
	 * Méthode qui initialise le jeu
	 */
	public abstract void start();
	
	/**
	 * Méthode qui effectue un tour dans le jeu
	 */
	public abstract void step();
	
	/**
	 * Méthode qui démarre le jeu tant qu'il y a des tours à effectuer
	 */
	public abstract void run();
	
	/**
	 * Méthode qui met en pause le jeu
	 */
	public abstract void stop();
	
	/**
	 * Méthode qui modifie le temps d'execution entre chaque tour de jeu.
	 */
	public abstract void setTime(long time);
	
	/**
	 * Fermer la fenetre du jeu si active
	 * @return
	 */
	public abstract void close();
	
	/**
	 * Ouvrir le jeu
	 */
	public abstract void open();

	/**
	 * @return the game
	 */
	public Game<T> getGame() {
		return game;
	}

	/**
	 * @param game the game to set
	 */
	public void setGame(Game<T> game) {
		this.game = game;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((game == null) ? 0 : game.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Controller<?> other = (Controller<?>) obj;
		if (game == null) {
			if (other.game != null)
				return false;
		} else if (!game.equals(other.game))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Controller [game=" + game + "]";
	}
}