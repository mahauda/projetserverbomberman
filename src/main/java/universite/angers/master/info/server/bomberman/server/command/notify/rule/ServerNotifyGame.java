package universite.angers.master.info.server.bomberman.server.command.notify.rule;

import java.net.Socket;
import java.util.Date;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.communicator.Communicator;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.socket.SocketService;
import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.api.Account;
import universite.angers.master.info.server.bomberman.model.api.Game;
import universite.angers.master.info.server.bomberman.model.api.User;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;
import universite.angers.master.info.server.bomberman.server.api.AccountAPI;
import universite.angers.master.info.server.bomberman.server.api.UserAPI;

/**
 * Classe qui permet d'informer à la vue coté client si le jeu continue toujours
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyGame implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyGame.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify game continue");
		
		Object[] args = (Object[])arg;
		LOG.debug("Args : " + args);
		
		//On récupère la communication
		@SuppressWarnings("unchecked")
		Communicator<String, Player> com = (Communicator<String, Player>)args[0];
		LOG.debug("Com : " + com);
		
		//On récupère la socket service
		@SuppressWarnings("unchecked")
		SocketService<String, Player> socketService = (SocketService<String, Player>)com.getEntityA();
		LOG.debug("Socket Service : " + socketService);
		
		//On récupère la socket Java
		Socket socket = socketService.getSocket();
		LOG.debug("Socket : " + socket);
		
		//On récupère l'identifiant formé du port + ip
		String id = String.valueOf(socket.getPort()) + socket.getInetAddress().toString();
		LOG.debug("Id : " + id);

		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if(args[1] == null) {
			player = new Player();
		} else {
			player = (Player)args[1];
		}
		
		boolean gameContinue = ServerBombermanGame.getInstance().getControllerBombermanGame().getGame().isGameContinue();
		LOG.debug("Game continue : " + gameContinue);
		
		player.setGameContinue(gameContinue);
		
		boolean gameOver = ServerBombermanGame.getInstance().getControllerBombermanGame().getGame().isGameOver();
		LOG.debug("Game over : " + gameOver);
		
		player.setGameOver(gameOver);
		
		//Si le jeu est terminé dans ce cas
		if(!gameContinue) {
			//On enregistre le résultat de la partie pour le joueur
			this.createNewGame(id);

			//On récupère la nouvelle map à afficher coté client
			String filename = Map.getInstance().getFilename();
			LOG.debug("File name : " + filename);
			
			player.setPathMap(filename);
		}
		
		player.setCommand("NOTIFY_GAME");
		LOG.debug("Player : " + player);
		
		return player;
	}
	
	/**
	 * Enregistre la partie dans le JEE
	 * @param id
	 */
	private void createNewGame(String id) {
		//On récupère le joueur par l'ID dans la map du serveur
		Player player = ServerBombermanGame.getInstance().getPlayers().get(id);
		LOG.debug("Player : " + player);
		
		//On récupère l'utilisateur via l'API
		LOG.debug("Login user : " + player.getLogin());
		User user = UserAPI.getInstance().read("pseudo", player.getLogin());
		LOG.debug("User : " + user);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			LOG.error(e.getMessage(), e);
		}
		
		//On récupère le compte associé à l'utilisateur via l'API grâce à l'identifiant du user
		LOG.debug("ID user : " + user.getId());
		Account account = AccountAPI.getInstance().read("id", user.getId());
		LOG.debug("Account : " + account);
		
		/**
		 * On crée une nouvelle partie
		 */
		
		//D'abord on récupère le score, qui est dans le bomberman
		CapacityItem itemPoint = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.POINT);
		LOG.debug("Item point : " + itemPoint);
		
		int scorePoint = itemPoint.getCapacityActualItem();
		LOG.debug("Score point : " + scorePoint);
		
		//Puis le niveau, qui est dans le singleton Map
		String level = Map.getInstance().getFilename();
		LOG.debug("Level : " + level);
		
		//Enfin le booléen s'il a perdu ou gagné
		boolean lose = ServerBombermanGame.getInstance().getControllerBombermanGame().getGame().isGameOver();
		LOG.debug("Lose : " + lose);
		
		//On crée la partie
		Game game = new Game(scorePoint, level, new Date(), lose);
		LOG.debug("Game : " + game);
		
		//On ajoute la partie dans la liste des jeux du compte
		//Soit dans la liste des jeux perdus
		//Soit dans la liste des jeux gagnés
		if(lose)
			account.getGamesLost().add(game);
		else
			account.getGamesWon().add(game);
		
		//On met à jour le compte avec l'API
		AccountAPI.getInstance().update(account);
	}
}
