package universite.angers.master.info.server.bomberman.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.controller.checkable.Checkable;
import universite.angers.master.info.server.bomberman.controller.checkable.RuleType;
import universite.angers.master.info.server.bomberman.ia.search.Node;
import universite.angers.master.info.server.bomberman.ia.solve.Solveable;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;

/**
 * Classe abstraite qui hérite Observable et implémente Runnable pour :
 * - Jouer le rôle de sujet afin de notifier les observateurs (les vues) qu'elle a changée d'état à l'aide du pattern Observateur ;
 * - Créer une architecture générale d’un jeu séquentiel à l’aide du pattern Patron de méthode ;
 * - Garder la main sur l’interface graphique pendant que le jeu s’éxécute en tâche de fond, comme le contrôle du temps de la simulation du jeu,
 *   ainsi que de lancer un grand nombre de simulations de jeu en parallèle pour évaluer différentes stratégies de comportement des agents.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class Game<T extends Solveable> implements Runnable, Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(Game.class);
	
	/**
	 * Les règles du jeu
	 */
	protected Map<RuleType, Checkable> rules;
	
	/**
	 * La solution étape par étape
	 */
	protected LinkedList<Node<T>> soluces;
	
	/**
     * Compter le nombre de tour courant
     */
	protected int turn;
	
	/**
     * Le nombre de tour maximum
     */
	protected int maxturn;
	
	/**
     * Savoir si le jeu est lancé
     * Jeu lancé = true
     * Jeu en pause = false
     */
	protected boolean isRunning;
	
	/**
     * Thread du jeu
     */
	protected Thread thread;
	
	/**
     * Corresponds au temps de pause entre chaque tour en millisecondes.
     */
	protected long time;
	
	/**
	 * Faut-il résoudre le jeu avec une IA ?
	 */
	protected boolean soluce;

	/**
     * Constructeur de Game
     * @param maxturn le nombre de tour maximum
     * @param time le temps d'execution entre chaque tour
     */
	public Game(int maxturn, long time) {		
		this.maxturn = maxturn;
		this.time = time;
		this.rules = new HashMap<>();
		this.soluces = new LinkedList<>();
	}
	
	/**
     * Méthode qui permet d'intialiser le jeu
     */
	public abstract void initializeGame();
	
	/**
     * Traitement à faire entre chaque tour
     */
	public void takeTurn(boolean soluce) {
		LOG.debug("Take a turn with soluce : " + soluce);
		
		//On récupère la solution trouvé par l'IA pour ce tour
		Node<T> node = null;
		
		if(!this.soluces.isEmpty())
			node = this.soluces.removeFirst();
		
		//Si on souhaite connaitre la solution pour ce tour alors on l'effectue
		if(soluce) {
			this.findSoluce(node);
		}
		
		//On execute toutes les règles du jeu à chaque tour si possible
		for(Checkable rule : this.rules.values()) {
			if(rule.checkRule())
				rule.executeRule();
		}
		
		//On change de map si gagné
		if(!this.isGameContinue()) {
			if(!this.isGameOver()) {
				LOG.debug("Next level");
				universite.angers.master.info.server.bomberman.model.bomberman.map.Map.getInstance().nextLevel();
			}
		}
		
		//On notifie tous les clients l'impact de l'exécution de toutes ces règles
		ServerBombermanGame.getInstance().getServer().notifyAllObservers("NOTIFY_TURN", null);
		
		//Si la partie est terminé dans ce cas on relance une nouvelle partie après avoir
		//notifié tous les joueurs
		if(!this.isGameContinue())
			ServerBombermanGame.getInstance().getControllerBombermanGame().open();
	}
	
	/**
	 * Effectuer la solution trouvé
	 * @param node
	 */
	public abstract void findSoluce(Node<T> node);
	

	public abstract boolean isGameOver();

	public abstract boolean isGameContinue();
	
	/**
	 * Méthode qui initialise le jeu en remettant le compteur du nombre de tours turn à zéro, 
	 * met isRunning à la valeur false et appelle la méthode abstraite void initializeGame(), 
	 * non implémentée pour l’instant. Elle sera implémentée par les classes concrètes qui héritent de Game.
	 */
	public void init() {
		// au debut y pas de tours
		this.turn = 0;
		
		// au debut le jeu n'est pas en route
		this.isRunning = false;
			
		this.initializeGame();
	}
	
	/**
	 * Méthode qui incrémente le compteur de tour du jeu et effectue
	 * un seul tour du jeu en appelant la méthode abstraite void takeTurn() si le jeu n’est pas
	 * terminé. Si le jeu est terminé, elle met le booléen isRunning à la valeur false
	 * puis fait appel à la méthode abstraite void gameOver() et qui permettra d’afficher un
	 * message de fin du jeu. Pour savoir si le jeu est terminé, elle appel la méthode abstraite
	 * boolean gameContinue() et vérifie si le nombre maximum de tours est atteint.
	 */
	public void step(boolean soluce) {
		LOG.debug("Step game with soluce : " + soluce);
		
		this.turn++;				
		this.takeTurn(soluce);
	}
	
	/**
	 * Méthode qui lance le jeu en pas à pas avec la méthode step() jusqu’à
	 * la fin tant que le jeu n’est pas mis en pause (testé par un flag booléen isRunning).
	 */
	@Override
	public void run() {
		LOG.debug("Run game");
		while(this.isRunning) {
			//Pause pour réguler l'execution du jeu entre chaque tour
			try {
				Thread.sleep(this.time);
				
				//Puis execute le tour
				this.step(this.soluce);
			} 
			catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}		
	}
	
	/**
	 * Méthode concrète stop() qui met en pause le jeu en mettant le flag booléen isRunning à false.
	 */
	public void stop() {
		this.isRunning = false;
	}
	
	/**
	 * Arreter le jeu
	 */
	public void close() {
		this.isRunning = false;
		
		if(this.thread != null)
			this.thread.interrupt();
	}
	
	/**
	 * Méthode qui met l’attribut isRunning à la valeur true puis instancie l’attribut thread 
	 * avec un nouvel objet Thread qui contient le jeu courant (thread = new Thread(this);) 
	 * et enfin fait appel à la méthode start() de cet objet thread pour lancer le jeu. 
	 * Remarque : l’appel à cette méthode lancera automatiquement la méthode run().
	 */
	public void launch(boolean soluce) {
		LOG.debug("Launch game with soluce : " + soluce);
		
		this.soluce = soluce;
		this.isRunning = true;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * @return the turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}

	/**
	 * @return the maxturn
	 */
	public int getMaxturn() {
		return maxturn;
	}

	/**
	 * @param maxturn the maxturn to set
	 */
	public void setMaxturn(int maxturn) {
		this.maxturn = maxturn;
	}

	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * @param isRunning the isRunning to set
	 */
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the thread
	 */
	public Thread getThread() {
		return thread;
	}

	/**
	 * @param thread the thread to set
	 */
	public void setThread(Thread thread) {
		this.thread = thread;
	}

	/**
	 * @return the rules
	 */
	public Map<RuleType, Checkable> getRules() {
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(Map<RuleType, Checkable> rules) {
		this.rules = rules;
	}

	/**
	 * @return the soluces
	 */
	public LinkedList<Node<T>> getSoluces() {
		return soluces;
	}

	/**
	 * @param soluces the soluces to set
	 */
	public void setSoluces(LinkedList<Node<T>> soluces) {
		this.soluces = soluces;
	}

	/**
	 * @return the soluce
	 */
	public boolean isSoluce() {
		return soluce;
	}

	/**
	 * @param soluce the soluce to set
	 */
	public void setSoluce(boolean soluce) {
		this.soluce = soluce;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isRunning ? 1231 : 1237);
		result = prime * result + maxturn;
		result = prime * result + ((rules == null) ? 0 : rules.hashCode());
		result = prime * result + (soluce ? 1231 : 1237);
		result = prime * result + ((soluces == null) ? 0 : soluces.hashCode());
		result = prime * result + ((thread == null) ? 0 : thread.hashCode());
		result = prime * result + (int) (time ^ (time >>> 32));
		result = prime * result + turn;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game<?> other = (Game<?>) obj;
		if (isRunning != other.isRunning)
			return false;
		if (maxturn != other.maxturn)
			return false;
		if (rules == null) {
			if (other.rules != null)
				return false;
		} else if (!rules.equals(other.rules))
			return false;
		if (soluce != other.soluce)
			return false;
		if (soluces == null) {
			if (other.soluces != null)
				return false;
		} else if (!soluces.equals(other.soluces))
			return false;
		if (thread == null) {
			if (other.thread != null)
				return false;
		} else if (!thread.equals(other.thread))
			return false;
		if (time != other.time)
			return false;
		if (turn != other.turn)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Game [rules=" + rules + ", soluces=" + soluces + ", turn=" + turn + ", maxturn=" + maxturn
				+ ", isRunning=" + isRunning + ", thread=" + thread + ", time=" + time + ", soluce=" + soluce + "]";
	}
}