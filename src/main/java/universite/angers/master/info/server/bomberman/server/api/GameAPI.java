package universite.angers.master.info.server.bomberman.server.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestObjectToString;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.api.MimeType;
import universite.angers.master.info.network.server.Request;
import universite.angers.master.info.server.bomberman.model.api.Game;

/**
 * API Game
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class GameAPI {

	private static final String URL_GAME_API = "http://localhost:8080/project-web-jee-bomberman/api/game";
	private static APIRestObjectToString<Game> gameAPI;

	private GameAPI() {

	}

	public static synchronized APIRestObjectToString<Game> getInstance() {
		if (gameAPI == null) {
			Request request = new Request(URL_GAME_API, Charset.UTF_8, MimeType.JSON);
			gameAPI = APIRestFactory.getAPIRestJsonToObject(Charset.UTF_8, request,
					new TypeToken<Game>() {}, new TypeToken<Collection<Game>>() {});
		}

		return gameAPI;
	}
}
