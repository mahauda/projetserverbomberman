package universite.angers.master.info.server.bomberman.controller.checkable;

import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Vérifier si un ennemi a attaqué un agent bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CheckRuleAttackBomberman extends CheckRule {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean checkRule() {
		return !Map.getInstance().getAgentsEnnemy().isEmpty();
	}
	
	@Override
	public void executeRule() {
		//Pour chaque ennemi, on attaque le bomberman
		for(AgentEnnemy ennemy : Map.getInstance().getAgentsEnnemy()) {
			
			ennemy.doAction(AgentAction.ATTACK);
			
			//Si le bomberman est mort, dans ce cas on le supprime
			if(Map.getInstance().getCurrentAgentKind().isDead()) {
				Map.getInstance().getAgents().remove(Map.getInstance().getCurrentAgentKind());
				Map.getInstance().getAgentsKind().remove(Map.getInstance().getCurrentAgentKind());
				Map.getInstance().getStart_Agents().remove(Map.getInstance().getCurrentAgentKind());	
			}
		}
		
		//On donne le nouvel état du bomberman
		// a la fin "notifyatackbomb"
		//ServerBombermanGame.getInstance().getServer().notifyAllObservers("NOTIFY_ATTACK_BOMBERMAN", null)
	}
}