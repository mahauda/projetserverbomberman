package universite.angers.master.info.server.bomberman.controller.checkable;

import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveableJump;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveableStill;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemyBird;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Vérifier si un agent bomberman se rapproche d'un agent bird
 * Si oui l'oiseau s'éloigne
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CheckRuleComingBombermanToBird extends CheckRule {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean checkRule() {
		for(AgentEnnemy agent : Map.getInstance().getAgentsEnnemy()) {
			if(agent instanceof AgentEnnemyBird) return true;
		}
		
		return false;
	}
	
	@Override
	public void executeRule() {
		for(AgentEnnemy agent : Map.getInstance().getAgentsEnnemy()) {
			if(!(agent instanceof AgentEnnemyBird)) continue;
			
			for(AgentKind kind : Map.getInstance().getAgentsKind()) {
				this.flyOrStillBird((AgentEnnemyBird)agent, kind);
			}
		}
	}
	
	/**
	 * Deux stratégies :
	 * - Soit il s'éloigne en sautant par dessus les murs
	 * - Soit l'oiseau reste immobile dans le cas où il n'y a pas d'agent bomberman
	 * @param bird
	 * @param kind
	 */
	private void flyOrStillBird(AgentEnnemyBird bird, AgentKind kind) {
		//On récupère les coordonnées de l'agent bird
		int agentBirdX = bird.getX();
		int agentBirdY = bird.getY();
		
		//On récupère les coordonnées de l'agent bomberman
		int agentBombX = kind.getX();
		int agentBombY = kind.getY();
		
		//Périmétre où il faut réveiller le bird
		int range = 2;
		
		//Si l'agent bomberman est dans le perimètre de l'agent bird, dans ce cas bird peut se déplacer
		//Sinon bird reste immobile
		if(Math.abs(agentBirdX - range) <= agentBombX && agentBombX <= Math.abs(agentBirdX + range)
				&& Math.abs(agentBirdY - range) <= agentBombY && agentBombY <= Math.abs(agentBirdY + range)) {
			
			//On change de stratégie de déplacement pour l'agent bird : il peut se déplacer en sautant par dessus les murs
			bird.setMove(new AgentMoveableJump());
			
		} else {
			//Sinon il reste immobile
			bird.setMove(new AgentMoveableStill());
		}
	}
}
