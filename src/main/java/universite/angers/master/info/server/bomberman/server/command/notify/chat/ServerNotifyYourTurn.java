package universite.angers.master.info.server.bomberman.server.command.notify.chat;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet d'informer au joueur courant de joueur son tour
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyYourTurn implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyYourTurn.class);

	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify Your Turn");
		Player playermessage = new Player();
		
		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if (arg == null) {
			player = new Player();
		} else {
			player = (Player) arg;
		}
		
		// On indique au joueur que c'est a lui de jouer
		playermessage.getMessages().add("[Serveur] : C'est à vous de jouer " +  player.getLogin());
		LOG.debug("[Serveur] : C'est à vous de jouer " +  player.getLogin());
		
		playermessage.setCommand("NOTIFY_YOUR_TURN");
		LOG.debug("Player : " + playermessage);
		
		return playermessage;
	}
}
