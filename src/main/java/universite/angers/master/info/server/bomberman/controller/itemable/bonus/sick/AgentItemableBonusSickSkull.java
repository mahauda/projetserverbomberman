package universite.angers.master.info.server.bomberman.controller.itemable.bonus.sick;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Rend malade pendant x tours (flag isSick à activer pour l’affichage).
 * Lorsqu’il est malade l’agent ne peut pas poser de bombes.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusSickSkull extends AgentItemableBonusSick {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusSickSkull() {
		
	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		return itemType == ItemType.SKULL;
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		bomberman.setSick(true);
	}
}
