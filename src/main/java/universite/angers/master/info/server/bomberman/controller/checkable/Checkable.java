package universite.angers.master.info.server.bomberman.controller.checkable;

/**
 * Effectuer une vérification d'une regle de jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Checkable {
	
	/**
	 * Vérifier si une règle peut être exécuter
	 * @return
	 */
	public boolean checkRule();
	
	/**
	 * La règle à exécuter
	 */
	public void executeRule();
}
