package universite.angers.master.info.server.bomberman.server.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestObjectToString;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.api.MimeType;
import universite.angers.master.info.network.server.Request;
import universite.angers.master.info.server.bomberman.model.api.User;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class UserAPI {

	private static final String URL_USER_API = "http://localhost:8080/project-web-jee-bomberman/api/user";
	private static APIRestObjectToString<User> userAPI;

	private UserAPI() {

	}

	public static synchronized APIRestObjectToString<User> getInstance() {
		if (userAPI == null) {
			Request request = new Request(URL_USER_API, Charset.UTF_8, MimeType.JSON);
			userAPI = APIRestFactory.getAPIRestJsonToObject(Charset.UTF_8, request,
					new TypeToken<User>() {}, new TypeToken<Collection<User>>() {});
		}

		return userAPI;
	}
}
