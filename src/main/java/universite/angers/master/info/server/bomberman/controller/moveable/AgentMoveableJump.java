package universite.angers.master.info.server.bomberman.controller.moveable;

import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Sauter/voler au dessus des murs
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentMoveableJump extends AgentMoveable {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {		
		if(agent == null) return false;
		if(move == null) return false;
		
		int xAgent = agent.getX();
		int yAgent = agent.getY();
		
		int xMap = Map.getInstance().getSize_x();
		int yMAp = Map.getInstance().getSize_y();

		//Un agent peut sauter, voler au dessus des murs mais sur les murs du bord		
		switch(move) {
			case MOVE_LEFT:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent-1][yAgent]) {					
					return xAgent >= 3 && Map.getInstance().getStart_brokable_walls()[xAgent-1][yAgent] && !Map.getInstance().getStart_brokable_walls()[xAgent-2][yAgent];
				}else {					
					return super.isLegalMove(agent, move);	
				}
			case MOVE_RIGHT:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent+1][yAgent]) {					
					return xAgent < xMap-3 && Map.getInstance().getStart_brokable_walls()[xAgent+1][yAgent] && !Map.getInstance().getStart_brokable_walls()[xAgent+2][yAgent];
				}else {
					return super.isLegalMove(agent, move);
				}				
			case MOVE_UP:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent][yAgent-1]) {
					return yAgent >= 3 && Map.getInstance().getStart_brokable_walls()[xAgent][yAgent-1] && !Map.getInstance().getStart_brokable_walls()[xAgent][yAgent-2];
				}else {
					return super.isLegalMove(agent, move);
				}				
			case MOVE_DOWN:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent][yAgent+1]) {					
					return yAgent < yMAp-3 && Map.getInstance().getStart_brokable_walls()[xAgent][yAgent+1] && !Map.getInstance().getStart_brokable_walls()[xAgent][yAgent+2];
				}else {					
					return super.isLegalMove(agent, move);
				}
			case STOP:				
				return false;
			default:
				System.out.println("pas de movement IS LEGAL");
				return false;
		}	
		
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		if(agent == null) return;
		if(move == null) return;
		
		int xAgent = agent.getX();
		int yAgent = agent.getY();
		
		switch(move) {
			case MOVE_LEFT:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent-1][yAgent]) {
					agent.setX(xAgent-2);
					agent.setAgentMove(move);
				}else {					
					super.doMove(agent, move);	
				}				
				break;
			case MOVE_RIGHT:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent+1][yAgent]) {									
					agent.setX(xAgent+2);
					agent.setAgentMove(move);
				}else {					
					super.doMove(agent, move);	
				}				
				break;
			case MOVE_UP:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent][yAgent-1]) {									
					agent.setY(yAgent-2);
					agent.setAgentMove(move);
				}else {					
					super.doMove(agent, move);	
				}				
				break;
			case MOVE_DOWN:				
				if(Map.getInstance().getStart_brokable_walls()[xAgent][yAgent+1]) {					
					agent.setY(yAgent+2);
					agent.setAgentMove(move);
				}else {					
					super.doMove(agent, move);	
				}				
				break;
			case STOP:				
				break;
			default:				
				System.out.println("pas de movement");
		}				
	}
}
