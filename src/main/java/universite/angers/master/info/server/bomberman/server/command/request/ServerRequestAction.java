package universite.angers.master.info.server.bomberman.server.command.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;

/**
 * Classe qui permet de passer la main à un autre joueur après chaque action enregistré d'un joueur
 * En effet chaque joueur effectue une action chacun dans l'ordre d'arrivé
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class ServerRequestAction implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerRequestAction.class);

	/**
	 * Savoir si c'est au joueur de jouer son coup
	 * @param playerClient
	 * @return
	 */
	protected boolean isPlayerTurn(Player playerClient) {
		LOG.debug("Player client : " + playerClient);
		
		if(playerClient == null) return false;
		
		/**
		 * Recuperer le joueur coté serveur
		 */
		Player playerServer = ServerBombermanGame.getInstance().getPlayers().get(playerClient.getId());
		LOG.debug("Player server  :" + playerServer);

		if (playerServer == null) return false;

		/**
		 * On verifie si c'est alui de jouer
		 */
		boolean isTurn = playerServer.isPlayTurn();
		LOG.debug("Is turn  : " + isTurn);

		return isTurn;
	}

	/**
	 * Donner la main au joueur suivant.
	 * @param player
	 * @param playersList la liste ordonnée par ordre d'arrivé des joueurs dans la partie
	 * @return
	 */
	private Player getNextPlayer(Player player, List<Player> playersList) {
		LOG.debug("Player : " + player);
		LOG.debug("Player list : " + playersList);
		
		if(player == null) return null;
		if(playersList == null || playersList.isEmpty()) return null;
		
		Player playerNext = null;
		for (int i = 0; i < playersList.size(); i++) {
			//On récupère le joueur dans la liste du serveur
			Player playerServeur = playersList.get(i);
			LOG.debug("Player serveur : " + playerServeur);
			
			//Si le joueur présent dans la liste est le joueur courant
			if (playerServeur.getId().equals(player.getId())) {
				
				//Alors on passe la main au joueur suivant dans la liste
				if (i < playersList.size() - 1) {
					playerNext = playersList.get(i + 1);
				} 
				//Sinon on revient au premier joueur
				else {
					playerNext = playersList.get(0);
				}
				
				//Si le joueur suivant vient de se déconnecter alors on donne la main
				//suivante au joueur après
				if (!playerNext.isConnected()) {
					playerNext = this.getNextPlayer(playerNext, playersList);
				}
				
				break;
			}
		}
		
		LOG.debug("Player next : " + playerNext);
		return playerNext;
	}

	/**
	 * Fonction qui permet de passer la main a un autre joueur
	 */
	protected void passTurnToOtherPlayer(Player playerClient) {
		LOG.debug("Player client : " + playerClient);
		
		if(playerClient == null) return;
	
		Player playerserver = ServerBombermanGame.getInstance().getPlayers().get(playerClient.getId());
		LOG.debug("Player serveur : " + playerserver);
		
		if(playerserver == null) return;
		
		// Ce nest plus au joueur courant de jouer et on passe la main a un autre
		// joueur dans la boucle
		playerserver.setPlayTurn(false);

		Collection<Player> playersCol = ServerBombermanGame.getInstance().getPlayers().values();
		LOG.debug("playersCol :" + playersCol);

		List<Player> playersList = new ArrayList<>(playersCol);
		LOG.debug("playersList :" + playersList);
		
		//On trie la liste par ordre d'arrivé
		Collections.sort(playersList, new Comparator<Player>() {

			@Override
			public int compare(Player player1, Player player2) {

				return player1.getArrival().compareTo(player2.getArrival());
			}
		});
		LOG.debug("playersListSort :" + playersList);

		// On parcours la liste des joueur triée et on affecte au joueur le tour si
		// c'est a lui de jouer
		
		Player playerNext = this.getNextPlayer(playerClient, playersList);
		LOG.debug("PlayerNext :" + playerNext);
		
		if (playerNext == null) return;
		
		playerNext.setPlayTurn(true);
		
		ServerBombermanGame.getInstance().notifyAllObserversWithoutPlayer(playerserver, "NOTIFY_PLAYER_TURN", playerNext);
		ServerBombermanGame.getInstance().notifyObserverOnlyOnePlayer(playerNext, "NOTIFY_YOUR_TURN", playerNext);
	}
}
