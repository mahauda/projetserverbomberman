package universite.angers.master.info.server.bomberman.util;

/**
 * Classe utilitaire pour le serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerUtil {

	private ServerUtil() {
		
	}
	
	/**
	 * Vérifier si une chaine est vide
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.trim().isEmpty())
            return false;
        return true;
    }
}
