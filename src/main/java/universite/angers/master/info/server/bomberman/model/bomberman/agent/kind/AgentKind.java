package universite.angers.master.info.server.bomberman.model.bomberman.agent.kind;

import java.util.HashMap;
import universite.angers.master.info.server.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.controller.itemable.Itemable;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.InfoAgent;

/**
 * Agent gentil qui peut posséder des bonus ou malus
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class AgentKind extends Agent {

	private static final long serialVersionUID = 1L;

	/**
	 * Toutes les bonus réalisables par les agents gentils
	 */
	protected transient java.util.Map<ItemType, Itemable> bonus;

	public AgentKind(String identifiant, String name, int x, int y, AgentMove move, char type, ColorAgent color,
			boolean isInvincible, boolean isSick, StateBomb stateBomb) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick);
		this.bonus = new HashMap<>();
		this.stateBomb = stateBomb;
	}

	public AgentKind(InfoAgent infoAgent, StateBomb stateBomb) {
		super(infoAgent);
		this.bonus = new HashMap<>();
		this.stateBomb = stateBomb;
	}

	/**
	 * Le bonus à réaliser sur le bomberman
	 * 
	 * @param agentAction
	 */
	public boolean isLegalBonus(ItemType itemType) {
		if (itemType == null)
			return false;
		if (!this.bonus.containsKey(itemType))
			return false;

		Itemable item = this.bonus.get(itemType);
		if (item == null)
			return false;

		return item.isLegalBonus(this, itemType);
	}

	/**
	 * Le bonus à réaliser sur le bomberman
	 * 
	 * @param agentAction
	 */
	public void doBonus(ItemType itemType) {
		if (itemType == null)
			return;
		if (!this.bonus.containsKey(itemType))
			return;

		Itemable item = this.bonus.get(itemType);
		if (item == null)
			return;

		if (item.isLegalBonus(this, itemType)) {
			item.doBonus(this, itemType);
		}
	}

	/**
	 * Vérifier si le bomberman est en vie Le bomberman est mort lorsqu'il n'a plus
	 * de vie
	 * 
	 * @return
	 */
	public boolean isDead() {
		CapacityItem life = this.getCapacityItem(ItemType.LIFE);
		if (life == null)
			return false;
		else
			return life.getCapacityActualItem() == 0;
	}

	/**
	 * @return the bonus
	 */
	public java.util.Map<ItemType, Itemable> getBonus() {
		return bonus;
	}

	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(java.util.Map<ItemType, Itemable> bonus) {
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "AgentKind [stateBomb=" + stateBomb + ", actions=" + actions + ", bonus=" + bonus + "]";
	}
}