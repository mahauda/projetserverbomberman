package universite.angers.master.info.server.bomberman.server.command.notify.rule;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Classe qui permet d'informer à la vue coté client les bonus appliqués au bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyApplyBonus implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyApplyBonus.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify move ennemy");
		
		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if(arg == null) {
			player = new Player();
		} else {
			player = (Player)arg;
		}
		
		/**
		 * Les bonus appliqués sur le bomberman
		 */
		player.setBomberman(Map.getInstance().getCurrentAgentKind());
		
		/**
		 * Les bonus utilisés qui faut peut etre supprimé
		 */
		player.setItems(Map.getInstance().getStart_Items());
		
		player.setCommand("NOTIFY_APPLY_BONUS");
		LOG.debug("Player : " + player);
		
		return player;
	}
}
