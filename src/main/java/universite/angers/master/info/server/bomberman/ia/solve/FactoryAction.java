package universite.angers.master.info.server.bomberman.ia.solve;

import java.util.ArrayList;
import java.util.List;

/**
 * Générer toutes les combinaisons d'actions à effectuer dans l'ordre
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactoryAction {

	private static List<List<Integer>> combinaisons = new ArrayList<>();

	private FactoryAction() {

	}

	public static void generateAllCombinaisons(int[] a, int k) {
		if (k == a.length) {
			List<Integer> combinaison = new ArrayList<>();
			for (int i = 0; i < a.length; i++) {
				combinaison.add(a[i]);
			}
			combinaisons.add(combinaison);
		} else {
			for (int i = k; i < a.length; i++) {
				int temp = a[k];
				a[k] = a[i];
				a[i] = temp;

				generateAllCombinaisons(a, k + 1);

				temp = a[k];
				a[k] = a[i];
				a[i] = temp;
			}
		}
	}

	/**
	 * @return the combinaisons
	 */
	public static List<List<Integer>> getCombinaisons() {
		return combinaisons;
	}

	/**
	 * @param combinaisons the combinaisons to set
	 */
	public static void setCombinaisons(List<List<Integer>> combinaisons) {
		FactoryAction.combinaisons = combinaisons;
	}
}
