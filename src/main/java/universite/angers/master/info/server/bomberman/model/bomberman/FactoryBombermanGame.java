package universite.angers.master.info.server.bomberman.model.bomberman;

import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleApplyBonus;
import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleAttackBomb;
import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleAttackBomberman;
import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleComingBombermanToBird;
import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleMoveAgentEnnemyRandomly;
import universite.angers.master.info.server.bomberman.controller.checkable.RuleType;

/**
 * Facotry pour construire le jeu en implémentant les règles
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactoryBombermanGame {

	/**
	 * Constructeur privé pour éviter l'instanciation de cette factory
	 */
	private FactoryBombermanGame() {
		
	}
	
	/**
	 * Retourne une instance de jeu pour le joueur
	 * Toutes les règles sont à enregistrés ici
	 * @param maxturn
	 * @param time
	 * @return
	 */
	public static BombermanGame getBombermanGameForPlayer(int maxturn, long time) {
		BombermanGame bombermanGame = new BombermanGame(maxturn, time);
		
		bombermanGame.getRules().put(RuleType.ATTACK_BOMB, new CheckRuleAttackBomb());
		bombermanGame.getRules().put(RuleType.ATTACK_ENNEMY, new CheckRuleAttackBomberman());
		bombermanGame.getRules().put(RuleType.APPLY_BONUS, new CheckRuleApplyBonus());
		bombermanGame.getRules().put(RuleType.FLY_BIRD, new CheckRuleComingBombermanToBird());
		//bombermanGame.getRules().put(RuleType.ENNEMY_MOVE_RANDOMLY, new CheckRuleMoveAgentEnnemyRandomly());
		
		return bombermanGame;
	}
	
	/**
	 * Retourne une instance de jeu pour la recherche en IA
	 * Seul les régles d'attaque sur les gentils et d'explosion des bombes sont activés
	 * En effet le déplacement aléatoire des ennemis restent compliqué à gérer
	 * @param maxturn
	 * @param time
	 * @return
	 */
	public static BombermanGame getBombermanGameForIA(int maxturn, long time) {
		BombermanGame bombermanGame = new BombermanGame(maxturn, time);
		
		bombermanGame.getRules().put(RuleType.ATTACK_BOMB, new CheckRuleAttackBomb());
		bombermanGame.getRules().put(RuleType.ATTACK_ENNEMY, new CheckRuleAttackBomberman());
		
		return bombermanGame;
	}
}
