package universite.angers.master.info.server.bomberman.ia.solve;

import java.util.List;

/**
 * L'interface Solveable représente une situation d'un problème à résoudre. Elle
 * doit être implémentée et redéfinir toutes les méthodes pour être utilisée
 * dans les algorithmes de recherche
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Solveable extends Cloneable {

	/**
	 * Permet de vérifier si au moins un opérateur est applicable pour générer des
	 * noeuds
	 * 
	 * @return true ou false
	 */
	public boolean checkActions();

	/**
	 * Permet de vérifier si un opérateur choisi par son index "choix" est
	 * applicable ou non
	 * 
	 * @param choix pour vérifier un opérateur
	 * @return true ou false
	 */
	public boolean checkActions(int action);

	/**
	 * Permet d'appliquer un opérateur choisi par son index "choix"
	 * 
	 * @param choix pour appliquer un opérateur
	 * @return le nom de l'opérateur qui aura été choisi
	 */
	public String applyActions(int action);

	/**
	 * Ensemble des actions à appliquer dans l'ordre qui ne contient aucun doublon
	 * 
	 * @return ensemble des actions
	 */
	public List<Integer> getActions();

	/**
	 * Effectuer une copie profonde de l'objet pour la recherche
	 * 
	 * @return une copie profonde
	 */
	public Solveable clone();
}
