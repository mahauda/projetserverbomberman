package universite.angers.master.info.server.bomberman.server.command.notify.rule;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Classe qui permet d'informer à la vue coté client les dégats subits par l'agent bomberman suite à une attaque d'ennemis
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyAttackBomberman implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyAttackBomberman.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify attack bomberman");

		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if(arg == null) {
			player = new Player();
		} else {
			player = (Player)arg;
		}
		
		//On donne le nouvel état du bomberman au joueur
		player.setBomberman(Map.getInstance().getCurrentAgentKind());
		
		player.setCommand("NOTIFY_ATTACK_BOMBERMAN");
		LOG.debug("Player : " + player);
		
		return player;
	}
}
