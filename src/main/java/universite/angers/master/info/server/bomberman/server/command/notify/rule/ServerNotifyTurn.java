package universite.angers.master.info.server.bomberman.server.command.notify.rule;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;

/**
 * Classe qui permet d'informer à la vue coté client les nouvelles informations du jeu à chaque tour
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyTurn implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyTurn.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify turn");

		Player player = new Player();
		Commandable<Player> notify;
		
		/**
		 * Ajout de bombe
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_ACTION_PUT_BOMB");
		notify.receive(player);
		
		/**
		 * Mouvement du bomberman
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_MOVE_BOMBERMAN");
		notify.receive(player);
		
		/**
		 * Mouvements aléatoires des ennemis
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_MOVE_ENNEMY");
		notify.receive(player);
		
		/**
		 * Les bonus
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_APPLY_BONUS");
		notify.receive(player);
		
		/**
		 * Les bombes
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_ATTACK_BOMB");
		notify.receive(player);
		
		/**
		 * Attack sur le bomberman
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_ATTACK_BOMBERMAN");
		notify.receive(player);
		
		/**
		 * L'état de la partie (en cours, perdu)
		 */
		notify = ServerBombermanGame.getInstance().getServer().getServerService().getNotifications().get("NOTIFY_GAME");
		notify.receive(new Object[] {arg, player});
		
		player.setCommand("NOTIFY_TURN");
		LOG.debug("Player : " + player);
		
		return player;
	}
}
