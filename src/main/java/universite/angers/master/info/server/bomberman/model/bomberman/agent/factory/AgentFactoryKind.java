package universite.angers.master.info.server.bomberman.model.bomberman.agent.factory;

import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.controller.actionnable.AgentActionnablePutBomb;
import universite.angers.master.info.server.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.controller.itemable.Itemable;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomb.AgentItemableBonusBombFire;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomb.AgentItemableBonusBombPut;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomberman.AgentItemableBonusBombermanSpeed;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.life.AgentItemableBonusFireSuit;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.life.AgentItemableBonusLifeHeart;
import universite.angers.master.info.server.bomberman.controller.itemable.bonus.sick.AgentItemableBonusSickSkull;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveable;
import universite.angers.master.info.server.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKindBomberman;

/**
 * Factory qui permet de créer des agents gentils
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentFactoryKind implements AgentFactory {
	
	private static AgentFactoryKind instance = null;
	
	private AgentFactoryKind() {}
	
	public static AgentFactoryKind getInstance() {
		if(instance == null)
			instance = new AgentFactoryKind();
		
		return instance;
	}
	
	@Override
	public Agent createAgent(String identifiant, String name, int x, int y, AgentMove move, char type, 
			ColorAgent color, boolean isInvincible, boolean isSick) {
		
		switch(type) {
			case AgentFactoryProvider.TYPE_AGENT_BOMBERMAN:
				AgentKindBomberman bomberman = new AgentKindBomberman(identifiant, name, x, y, move, type, color, isInvincible, isSick, StateBomb.Step1);
				
				//Les mouvements
				bomberman.setMove(new AgentMoveable());
				
				//Les actions
				bomberman.getActions().put(AgentAction.PUT_BOMB, new AgentActionnablePutBomb());
				
				//Les bonus
				this.attachBonus(bomberman);
				
				return bomberman;
				
			default:
				return null;
		}
	}	
	
	@Override
	public Agent createAgent(InfoAgent infoAgent) {

		switch(infoAgent.getType()) {
			case AgentFactoryProvider.TYPE_AGENT_BOMBERMAN:
				AgentKindBomberman bomberman = new AgentKindBomberman(infoAgent, StateBomb.Step1);
				
				//Les mouvements
				bomberman.setMove(new AgentMoveable());
				
				//Les actions
				bomberman.getActions().put(AgentAction.PUT_BOMB, new AgentActionnablePutBomb());
				
				//Les bonus
				this.attachBonus(bomberman);
				
				return bomberman;
				
			default:
				return null;
		}
	}
	
	private void attachBonus(AgentKindBomberman bomberman) {
		//Les bonus
		
		//VIE
		
		//0-->il peut mourrir
		CapacityItem itemLife = new CapacityItem(0, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.LIFE, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_UP, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_DOWN, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_FULL_UP, itemLife);
		bomberman.getCapacityItems().put(ItemType.LIFE_FULL_DOWN, itemLife);
		
		Itemable bonusLife = new AgentItemableBonusLifeHeart();
		bomberman.getBonus().put(ItemType.LIFE, bonusLife);
		bomberman.getBonus().put(ItemType.LIFE_UP, bonusLife);
		bomberman.getBonus().put(ItemType.LIFE_DOWN, bonusLife);
		bomberman.getBonus().put(ItemType.LIFE_FULL_UP, bonusLife);
		bomberman.getBonus().put(ItemType.LIFE_FULL_DOWN, bonusLife);
		
		//SPEED
		
		CapacityItem itemSpeed = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.SPEED, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_UP, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_DOWN, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_FULL_UP, itemSpeed);
		bomberman.getCapacityItems().put(ItemType.SPEED_FULL_DOWN, itemSpeed);
		
		Itemable bonusSpeed = new AgentItemableBonusBombermanSpeed();
		bomberman.getBonus().put(ItemType.SPEED, bonusSpeed);
		bomberman.getBonus().put(ItemType.SPEED_UP, bonusSpeed);
		bomberman.getBonus().put(ItemType.SPEED_DOWN, bonusSpeed);
		bomberman.getBonus().put(ItemType.SPEED_FULL_UP, bonusSpeed);
		bomberman.getBonus().put(ItemType.SPEED_FULL_DOWN, bonusSpeed);
		
		//BOMB
		
		CapacityItem itemBomb = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.BOMB, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_UP, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_DOWN, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_FULL_UP, itemBomb);
		bomberman.getCapacityItems().put(ItemType.BOMB_FULL_DOWN, itemBomb);
		
		Itemable bonusBomb = new AgentItemableBonusBombPut();
		bomberman.getBonus().put(ItemType.BOMB, bonusBomb);
		bomberman.getBonus().put(ItemType.BOMB_UP, bonusBomb);
		bomberman.getBonus().put(ItemType.BOMB_DOWN, bonusBomb);
		bomberman.getBonus().put(ItemType.BOMB_FULL_UP, bonusBomb);
		bomberman.getBonus().put(ItemType.BOMB_FULL_DOWN, bonusBomb);
		
		//EXPLOSION
		
		CapacityItem itemFire = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.FIRE, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_UP, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_DOWN, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_FULL_UP, itemFire);
		bomberman.getCapacityItems().put(ItemType.FIRE_FULL_DOWN, itemFire);
		
		Itemable bonusFire = new AgentItemableBonusBombFire();
		bomberman.getBonus().put(ItemType.FIRE, bonusFire);
		bomberman.getBonus().put(ItemType.FIRE_UP, bonusFire);
		bomberman.getBonus().put(ItemType.FIRE_DOWN, bonusFire);
		bomberman.getBonus().put(ItemType.FIRE_FULL_UP, bonusFire);
		bomberman.getBonus().put(ItemType.FIRE_FULL_DOWN, bonusFire);
		
		//INVISIBLE
		
		CapacityItem itemInvisible = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.FIRE_SUIT, itemInvisible);
		
		Itemable bonusInvisible = new AgentItemableBonusFireSuit();
		bomberman.getBonus().put(ItemType.FIRE_SUIT, bonusInvisible);
		
		//MALADE
		
		CapacityItem itemSick = new CapacityItem(1, 10, 1, 1);
		bomberman.getCapacityItems().put(ItemType.SKULL, itemSick);
		
		Itemable bonusSick = new AgentItemableBonusSickSkull();
		bomberman.getBonus().put(ItemType.SKULL, bonusSick);
		
		//POINTS
		
		//Il peut avoir des points infinis
		CapacityItem point = new CapacityItem(0, Integer.MAX_VALUE, 1, 1);
		bomberman.getCapacityItems().put(ItemType.POINT, point);
	}
}