package universite.angers.master.info.server.bomberman.controller.moveable;

import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;

/**
 * Un agent reste immobile
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentMoveableStill extends AgentMoveable {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean isLegalMove(Agent agent, AgentMove move) {
		return true;
	}

	@Override
	public void doMove(Agent agent, AgentMove move) {
		agent.setAgentMove(AgentMove.STOP);
	}
}
