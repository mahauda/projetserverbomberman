package universite.angers.master.info.server.bomberman.controller.actionnable;

import java.io.Serializable;

/**
 * La bombe
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class InfoBomb implements Serializable {

	private static final long serialVersionUID = 1L;
	private int x;
	private int y;
	private int range;	
	private StateBomb stateBomb;

	public InfoBomb(int x, int y, int range, StateBomb stateBomb) {
		this.x=x;
		this.y=y;
		this.range=range;
		this.stateBomb = stateBomb;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the range
	 */
	public int getRange() {
		return range;
	}

	/**
	 * @param range the range to set
	 */
	public void setRange(int range) {
		this.range = range;
	}

	/**
	 * @return the stateBomb
	 */
	public StateBomb getStateBomb() {
		return stateBomb;
	}

	/**
	 * @param stateBomb the stateBomb to set
	 */
	public void setStateBomb(StateBomb stateBomb) {
		this.stateBomb = stateBomb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + range;
		result = prime * result + ((stateBomb == null) ? 0 : stateBomb.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoBomb other = (InfoBomb) obj;
		if (range != other.range)
			return false;
		if (stateBomb != other.stateBomb)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoBomb [x=" + x + ", y=" + y + ", range=" + range + ", stateBomb=" + stateBomb + "]";
	}
}
	