package universite.angers.master.info.server.bomberman.ia.search;

import java.util.List;

import universite.angers.master.info.server.bomberman.ia.search.explorable.Explorable;
import universite.angers.master.info.server.bomberman.ia.solve.Problem;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.Heuristic;
import universite.angers.master.info.server.bomberman.model.bomberman.map.FactoryMap;
import universite.angers.master.info.server.bomberman.model.bomberman.map.MapAdapter;

/**
 * Frabrique l'arbre de recherche
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactorySearch {

	/**
	 * Constructeur privé afin d'éviter l'instanciation
	 */
	private FactorySearch() {

	}

	/**
	 * Construit l'arbre de recherche pour résoudre le problème bomberman
	 * 
	 * @param maxturn
	 * @param time
	 * @param filename
	 * @param frontier
	 * @param heuristic
	 * @return
	 */
	public static Node<MapAdapter> getSoluceBomberman(int maxturn, long time, String filename, List<Integer> actions,
			Explorable<MapAdapter> frontier, Heuristic<MapAdapter> heuristic) {
		// Etat initial
		MapAdapter bombermanInitial = FactoryMap.getMapIAIntial(maxturn, time, filename, actions);

		// Etats finaux : tous les agents sont morts et tous les bombermans peuvent être
		// sur n'importe où sur le plateau
		// On crée autant d'état qu'il existe de bomberman
		List<MapAdapter> bombermanFinals = FactoryMap.getListMapIAFinals(bombermanInitial);

		// Résolution du probleme
		Problem<MapAdapter> problem = new Problem<>(bombermanInitial, bombermanFinals);

		// Recherche
		Search<MapAdapter> search = new Search<>(problem, frontier, heuristic);

		return search.explore();
	}
}
