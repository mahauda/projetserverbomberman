package universite.angers.master.info.server.bomberman.game;

import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;

/**
 * Classe qui teste l'implémentation du jeu Bomberman en instanciant le modele BombermanGame 
 * et le controlleur ControllerBomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerMainBomberman {
	
	public static void main(String[] args) {
		ServerBombermanGame.getInstance().getServer().open();
	}
}
