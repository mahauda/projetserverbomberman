package universite.angers.master.info.server.bomberman.server;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.translater.json.TranslaterJsonFactory;
import universite.angers.master.info.api.translater.json.TranslaterJsonToObject;
import universite.angers.master.info.api.translater.json.TranslaterObjectToJson;
import universite.angers.master.info.network.communicator.Communicator;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.Server;
import universite.angers.master.info.network.server.ServerService;
import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.server.bomberman.controller.ControllerBombermanGame;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.FactoryBombermanGame;
import universite.angers.master.info.server.bomberman.server.command.notify.ServerNotifyDefault;
import universite.angers.master.info.server.bomberman.server.command.notify.ServerNotifyEcho;
import universite.angers.master.info.server.bomberman.server.command.notify.chat.ServerNotifyLogoutPlayer;
import universite.angers.master.info.server.bomberman.server.command.notify.chat.ServerNotifyNewPlayer;
import universite.angers.master.info.server.bomberman.server.command.notify.chat.ServerNotifyPlayerTurn;
import universite.angers.master.info.server.bomberman.server.command.notify.chat.ServerNotifyYourTurn;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyActionPutBomb;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyApplyBonus;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyAttackBomb;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyAttackBomberman;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyGame;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyMoveBomberman;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyMoveEnnemy;
import universite.angers.master.info.server.bomberman.server.command.notify.rule.ServerNotifyTurn;
import universite.angers.master.info.server.bomberman.server.command.request.ServerRequestDefault;
import universite.angers.master.info.server.bomberman.server.command.request.ServerRequestLogin;
import universite.angers.master.info.server.bomberman.server.command.request.ServerRequestLogout;
import universite.angers.master.info.server.bomberman.server.command.request.ServerRequestMoveBomberman;
import universite.angers.master.info.server.bomberman.server.command.request.ServerRequestActionPutBomb;

/**
 * Serveur du jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerBombermanGame {

	private static final Logger LOG = Logger.getLogger(ServerBombermanGame.class);
	
	/**
	 * Le host du serveur
	 */
	private static final String HOST = "127.0.0.1";
	
	/**
	 * Le port du serveur
	 */
	private static final int PORT = 2345;

	/**
	 * Une unique instance du jeu
	 */
	private static ServerBombermanGame serverBombermanGame;
	
	/**
	 * Le serveur du jeu
	 */
	private Server<Player> server;
	
	/**
	 * Les joueurs enregistrés
	 */
	private Map<String, Player> players;
	
	/**
	 * Le controlleur du jeu
	 */
	private ControllerBombermanGame controllerBombermanGame;

	public static synchronized ServerBombermanGame getInstance() {
		if(serverBombermanGame == null) {
			serverBombermanGame = new ServerBombermanGame();	
		}
		
		return serverBombermanGame;
	}
	
	private ServerBombermanGame() {
		this.players = new HashMap<>();
		
		//Service
		ServerService<Player> service = new ServerService<>(null, null, new ServerConverterCommandName(), new ReentrantWithoutReadWriteLock(true));	

		//On enregistre les notifications à envoyer aux clients sans retour de réponse
		service.registerNotification("NOTIFY_ACTION_PUT_BOMB", new ServerNotifyActionPutBomb());
		service.registerNotification("NOTIFY_PLAYER_TURN", new ServerNotifyPlayerTurn());
		service.registerNotification("NOTIFY_YOUR_TURN", new ServerNotifyYourTurn());
		service.registerNotification("NOTIFY_NEW_PLAYER", new ServerNotifyNewPlayer());
		service.registerNotification("NOTIFY_LOGOUT_PLAYER", new ServerNotifyLogoutPlayer());
		service.registerNotification("NOTIFY_MOVE_ENNEMY", new ServerNotifyMoveEnnemy());
		service.registerNotification("NOTIFY_MOVE_BOMBERMAN", new ServerNotifyMoveBomberman());
		service.registerNotification("NOTIFY_ATTACK_BOMBERMAN", new ServerNotifyAttackBomberman());
		service.registerNotification("NOTIFY_ATTACK_BOMB", new ServerNotifyAttackBomb());
		service.registerNotification("NOTIFY_GAME", new ServerNotifyGame());
		service.registerNotification("NOTIFY_APPLY_BONUS", new ServerNotifyApplyBonus());
		service.registerNotification("NOTIFY_TURN", new ServerNotifyTurn());
		service.registerNotification(Subject.NOTIFY_ECHO.getName(), new ServerNotifyEcho());
		service.registerNotification(Subject.NOTIFY_DEFAULT.getName(), new ServerNotifyDefault());
		
		//On enregistre les requetes venant du client qui pourront etre traité par le serveur
        service.registerRequest("REQUEST_LOGIN", new ServerRequestLogin());
        service.registerRequest("REQUEST_LOGOUT", new ServerRequestLogout());
        service.registerRequest("REQUEST_ACTION_PUT_BOMB", new ServerRequestActionPutBomb());
        service.registerRequest("REQUEST_MOVE_AGENT", new ServerRequestMoveBomberman());
        service.registerRequest("REQUEST_DEFAULT", new ServerRequestDefault());
        
        //On enregistre les notifications et requetes par défaut
        service.setRequestDefault(new ServerRequestDefault());
        service.setNotificationDefault(new ServerNotifyDefault());
        
        //On passe les traducteurs pour convertir un objet en json et inversement
        
        //OBJECT --> JSON
        TranslaterObjectToJson<Player> playerToJson = TranslaterJsonFactory.getTranslaterObjectToJson(new TypeToken<Player>() {});
        
        //JSON --> OBJECT
        TranslaterJsonToObject<Player> jsonToPlayer = TranslaterJsonFactory.getTranslaterJsonToObject(new TypeToken<Player>() {});
        
        this.server = new Server<>(HOST, PORT, 10, service, playerToJson, jsonToPlayer);
        
        this.start();
	}
	
	/**
	 * methode permettant de notifer tout les joueurs sauf le joueur passé au premier parametre
	 */
	public void notifyAllObserversWithoutPlayer(Player player, String subject, Object arg) {
		Communicator<String,Player > com = this.server.getCommunicatorServerServiceClients().get(player.getId());
		this.server.notifyAllObservers(Arrays.asList(com),subject , arg);
	
	}
	/**
	 * methode qui permet de notifier un seul joueur
	 */
	public void notifyObserverOnlyOnePlayer(Player player, String subject, Object arg) {
		Communicator<String,Player > com = this.server.getCommunicatorServerServiceClients().get(player.getId());
		this.server.notifyObserver(subject , com, arg);
	}
	/**
	 * Méthode qui permet de démarrer le jeu
	 */
	public void start() {
		this.controllerBombermanGame = new ControllerBombermanGame(FactoryBombermanGame.getBombermanGameForPlayer(1000, 1000));
	}
	
	public boolean addPlayer(Player player) {
		if(player == null) return false;
		
		if(!this.players.containsKey(player.getId())) {
			player.setArrival(new Date());
			player.setConnected(true);
			this.players.put(player.getId(), player);
			return true;
		} else return false;
	}
	
	public boolean removePlayer(Player player) {
		LOG.debug("Player : " + player);
		if(player == null) return false;
		
		LOG.debug("Login player : " + player.getLogin());
		player.setConnected(false);
		this.players.remove(player.getLogin());
		LOG.debug("Player remove in bomberman game");
			
		LOG.debug("ID player : " + player.getId());
		Communicator<String, Player> com = this.server.getCommunicatorServerServiceClients().remove(player.getId());
		com.close();
		LOG.debug("Com player remove in server");
		
		return true;
	}
	
	/**
	 * @return the server
	 */
	public Server<Player> getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server<Player> server) {
		this.server = server;
	}

	/**
	 * @return the players
	 */
	public Map<String, Player> getPlayers() {
		return players;
	}

	/**
	 * @param players the players to set
	 */
	public void setPlayers(Map<String, Player> players) {
		this.players = players;
	}

	/**
	 * @return the controllerBombermanGame
	 */
	public ControllerBombermanGame getControllerBombermanGame() {
		return controllerBombermanGame;
	}

	/**
	 * @param controllerBombermanGame the controllerBombermanGame to set
	 */
	public void setControllerBombermanGame(ControllerBombermanGame controllerBombermanGame) {
		this.controllerBombermanGame = controllerBombermanGame;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((controllerBombermanGame == null) ? 0 : controllerBombermanGame.hashCode());
		result = prime * result + ((players == null) ? 0 : players.hashCode());
		result = prime * result + ((server == null) ? 0 : server.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerBombermanGame other = (ServerBombermanGame) obj;
		if (controllerBombermanGame == null) {
			if (other.controllerBombermanGame != null)
				return false;
		} else if (!controllerBombermanGame.equals(other.controllerBombermanGame))
			return false;
		if (players == null) {
			if (other.players != null)
				return false;
		} else if (!players.equals(other.players))
			return false;
		if (server == null) {
			if (other.server != null)
				return false;
		} else if (!server.equals(other.server))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ServerBombermanGame [server=" + server + ", players=" + players + ", controllerBombermanGame="
				+ controllerBombermanGame + "]";
	}	
}
