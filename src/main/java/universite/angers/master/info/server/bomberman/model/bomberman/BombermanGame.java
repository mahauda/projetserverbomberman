package universite.angers.master.info.server.bomberman.model.bomberman;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.controller.actionnable.StateBomb;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.ia.search.Node;
import universite.angers.master.info.server.bomberman.model.Game;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.factory.AgentFactory;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.factory.AgentFactoryProvider;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.server.bomberman.model.bomberman.map.MapAdapter;

/**
 * Classe qui hérite Game et implémente les régles du jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class BombermanGame extends Game<MapAdapter> {

	private static final Logger LOG = Logger.getLogger(BombermanGame.class);
	private static final long serialVersionUID = 1L;

	public BombermanGame(int maxturn, long time) {
		super(maxturn, time);
	}

	@Override
	public void initializeGame() {
		// On charge la map avec le layout.lay.
		try {
			Map.getInstance().load();
		} 
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return;
		}	
		
		// Si la map n'est par chargé, on ne peut pas initialiser le jeu
		if (Map.getInstance().getStart_Agents().isEmpty())
			return;

		// On crée les différents agents avec le DP Factory
		for (InfoAgent infoAgent : Map.getInstance().getStart_Agents()) {
			AgentFactory agentFactory = AgentFactoryProvider.getAgentFactory(infoAgent.getType());
			Agent agent = agentFactory.createAgent(infoAgent);

			if (agent != null)
				Map.getInstance().getAgents().add(agent);

			if (agent instanceof AgentKind)
				Map.getInstance().getAgentsKind().add((AgentKind) agent);

			if (agent instanceof AgentEnnemy)
				Map.getInstance().getAgentsEnnemy().add((AgentEnnemy) agent);
		}

		// Pour l'instant on prend le premier bomberman
		Map.getInstance().setCurrentAgentKind(Map.getInstance().getAgentsKind().get(0));

		// On change le tableau de la map avec cette nouvelle liste d'agent
		// afin de les manipuler par référence
		Map.getInstance().setStart_Agents(new ArrayList<>(Map.getInstance().getAgents()));
	}

	@Override
	public void findSoluce(Node<MapAdapter> node) {
		if (node == null)
			return;

		// Si l'action est un mouvement
		String action = node.getAction();
		if (action == null || action.equals(""))
			return;

		AgentMove agentMove = null;
		AgentAction agentAction = null;

		try {
			agentMove = AgentMove.valueOf(action);
		} catch (IllegalArgumentException e) {

		}

		try {
			agentAction = AgentAction.valueOf(action);
		} catch (IllegalArgumentException e) {

		}

		if (agentMove != null) {
			Map.getInstance().getCurrentAgentKind().doMove(agentMove);
		}
		if (agentAction != null) {
			// Dans le cas de poser une bombe on la fait exploser immédiatement
			if (agentAction == AgentAction.PUT_BOMB) {
				StateBomb stateBomb = Map.getInstance().getCurrentAgentKind().getStateBomb();
				Map.getInstance().getCurrentAgentKind().setStateBomb(StateBomb.Boom);
				Map.getInstance().getCurrentAgentKind().doAction(agentAction);
				Map.getInstance().getCurrentAgentKind().setStateBomb(stateBomb);
			}
		}
	}

	/**
	 * La partie est terminé. 
	 * Soit parce que L'agent bomberman est mort 
	 * Soit le nombre de tour est hors limite
	 */
	@Override
	public boolean isGameOver() {
		return Map.getInstance().getCurrentAgentKind().isDead() || Map.getInstance().getAgentsKind().isEmpty()
				|| this.turn >= this.maxturn;
	}

	/**
	 * Le jeu continue tant qu'il y a des agents sur le plateau et que l'agent
	 * bomberman n'est pas mort et que le nombre de tour n'est pas arrivé au max
	 */
	@Override
	public boolean isGameContinue() {
		return !Map.getInstance().getAgentsEnnemy().isEmpty() && !Map.getInstance().getAgentsKind().isEmpty()
				&& !Map.getInstance().getCurrentAgentKind().isDead() && this.turn < this.maxturn;
	}
}
