package universite.angers.master.info.server.bomberman.model.bomberman.agent.factory;

import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.controller.actionnable.AgentActionnableAttack;
import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveable;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveableChase;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveableStill;
import universite.angers.master.info.server.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.InfoAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemyBasic;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemyBird;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemyRajion;

/**
 * Factory qui permet de créer des agents ennemis
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentFactoryEnnemy implements AgentFactory {
	
	private static AgentFactoryEnnemy instance = null;
	
	private AgentFactoryEnnemy() {}
	
	public static AgentFactoryEnnemy getInstance()
	{
		if(instance == null)
			instance = new AgentFactoryEnnemy();
		
		return instance;
	}
	
	@Override
	public Agent createAgent(String identifiant, String name, int x, int y, AgentMove move, char type, 
			ColorAgent color, boolean isInvincible, boolean isSick) {
		
		Agent agent = null;
		CapacityItem attack = null;
		
		switch(type) {
			case AgentFactoryProvider.TYPE_AGENT_BASIQUE:
				agent = new AgentEnnemyBasic(identifiant, name, x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 1
				attack = new CapacityItem(1, 10, 1, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveable());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_RAJION:
				agent = new AgentEnnemyRajion(identifiant, name, x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 2
				attack = new CapacityItem(2, 10, 2, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableChase());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_BIRD:
				agent = new AgentEnnemyBird(identifiant, name, x, y, move, type, color, isInvincible, isSick);
				//Attaque ayant un dégat au minimum de 3
				attack = new CapacityItem(3, 10, 3, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableStill());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			default:
				return agent;
		}
	}

	@Override
	public Agent createAgent(InfoAgent infoAgent) {
		
		Agent agent = null;
		CapacityItem attack = null;
		
		switch(infoAgent.getType()) {
			case AgentFactoryProvider.TYPE_AGENT_BASIQUE:
				agent = new AgentEnnemyBasic(infoAgent);
				//Attaque ayant un dégat au minimum de 1
				attack = new CapacityItem(1, 10, 1, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveable());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_RAJION:
				agent = new AgentEnnemyRajion(infoAgent);
				//Attaque ayant un dégat au minimum de 2
				attack = new CapacityItem(2, 10, 2, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableChase());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			case AgentFactoryProvider.TYPE_AGENT_BIRD:
				agent = new AgentEnnemyBird(infoAgent);
				//Attaque ayant un dégat au minimum de 3
				attack = new CapacityItem(3, 10, 3, 1);
				agent.getCapacityItems().put(ItemType.ATTACK, attack);
				//Les mouvements
				agent.setMove(new AgentMoveableStill());
				//Les actions
				agent.getActions().put(AgentAction.ATTACK, new AgentActionnableAttack());
				return agent;
			default:
				return agent;
		}
	}
}