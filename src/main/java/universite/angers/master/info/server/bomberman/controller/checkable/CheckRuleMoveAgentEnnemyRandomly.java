package universite.angers.master.info.server.bomberman.controller.checkable;

import java.util.Random;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Déplacer les ennemis aléatoirement sur le plateau
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class CheckRuleMoveAgentEnnemyRandomly extends CheckRule {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public boolean checkRule() {
		return !Map.getInstance().getAgentsEnnemy().isEmpty();
	}
	
	/**
	 * Déplacer les ennemis aléatoirements
	 */
	@Override
	public void executeRule() {
		for(AgentEnnemy ennemy : Map.getInstance().getAgentsEnnemy()) {
			AgentMove randomMove = this.choseRandomMove();
			ennemy.doMove(randomMove);
		}
		
		//On donne les nouvelles positions de chaque ennemis aux clients
		//ServerBombermanGame.getInstance().getServer().notifyAllObservers("NOTIFY_MOVE_ENNEMY", null)
	}

	/**
	 * Choisir une action aléatoirement
	 * @return l'action tirée aléatoirement
	 */
	private AgentMove choseRandomMove() {
		int maxAction = AgentMove.values().length;
		int randomMove = new Random().nextInt(maxAction);
		
		return AgentMove.values()[randomMove];
	}
}
