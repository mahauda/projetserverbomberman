package universite.angers.master.info.server.bomberman.server;

import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet de récupérer le nom de la commande dans un objet "Player"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerConverterCommandName implements Convertable<String, Player> {

	@Override
	public String convert(Player message) {
		return message.getCommand();
	}
}
