package universite.angers.master.info.server.bomberman.server.api;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.APIRestFactory;
import universite.angers.master.info.api.APIRestObjectToString;
import universite.angers.master.info.api.Charset;
import universite.angers.master.info.api.MimeType;
import universite.angers.master.info.network.server.Request;
import universite.angers.master.info.server.bomberman.model.api.Account;

/**
 * API User
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AccountAPI {

	private static final String URL_ACCOUNT_API = "http://localhost:8080/project-web-jee-bomberman/api/account";
	private static APIRestObjectToString<Account> accountAPI;

	private AccountAPI() {

	}

	public static synchronized APIRestObjectToString<Account> getInstance() {
		if (accountAPI == null) {
			Request request = new Request(URL_ACCOUNT_API, Charset.UTF_8, MimeType.JSON);
			accountAPI = APIRestFactory.getAPIRestJsonToObject(Charset.UTF_8, request,
					new TypeToken<Account>() {}, new TypeToken<Collection<Account>>() {});
		}

		return accountAPI;
	}
}
