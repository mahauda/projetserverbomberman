package universite.angers.master.info.server.bomberman.ia.solve.heuristic;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.ia.search.Node;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy.AgentEnnemy;
import universite.angers.master.info.server.bomberman.model.bomberman.map.MapAdapter;

/**
 * Fonction heuristique du bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class HeuristicBomberman extends Heuristic<MapAdapter> {

	public HeuristicBomberman() {

	}

	/**
	 * Ici on calcul la distance entre l'agent bomberman et l'agent ennemi le plus
	 * proche selon la distance de Manhattan ou Euclidienne Le but étant de tuer
	 * tous les ennemis
	 */
	@Override
	public double calculHHeuristic(Node<MapAdapter> n) {
		double distance = 0;
		double distanceMin = Integer.MAX_VALUE;
		Agent agentBomberman = n.getEtat().getCurrentAgentKind();

		// On récupère l'ensemble des distances entre le bomberman et les ennemis
		for (AgentEnnemy ennemy : n.getEtat().getAgentsEnnemy()) {
			distance = this.calculDistance(agentBomberman, ennemy);
			if (distance < distanceMin) {
				distanceMin = distance;
			}
		}

		this.h = distanceMin;
		return distanceMin;
	}

	/**
	 * Ici on le calcul les points que cela à rapporté à l'agent bomberman et le
	 * nombre de cout pour arriver à l'ennemi additionné au cout
	 */
	@Override
	public double calculGHeuristic(Node<MapAdapter> n) {
		CapacityItem point = n.getEtat().getCurrentAgentKind().getCapacityItem(ItemType.POINT);
		if (point == null)
			return 0;

		this.g = point.getCapacityActualItem() + n.getCout();
		return point.getCapacityActualItem() + n.getCout();
	}

	/**
	 * Calcul de la distance entre deux agents
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public abstract double calculDistance(Agent a, Agent b);
}
