package universite.angers.master.info.server.bomberman.controller.itemable;

import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Source : https://fr.wikipedia.org/wiki/Bomberman
 * Les jeux de la série des Bomberman proposent généralement tout un ensemble de bonus permettant 
 * d'améliorer les possibilités de son Bomberman. Ces bonus se trouvent la plupart du temps dans 
 * les blocs destructibles et apparaissent une fois ceux-ci détruits. On trouve ainsi des bonus 
 * permettant d'améliorer les bombes posées et d'autres améliorant les caractéristiques du bomberman. 
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Itemable {

	/**
	 * Vérifier si un bonus est réalisable par le bomberman
	 * @param agent
	 * @param action
	 * @return vrai si on peut. Faux dans le cas contraire
	 */
	public boolean isLegalBonus(AgentKind agentKind, ItemType itemType);
	
	/**
	 * Réaliser le bonus sur l'agent bomberman
	 * @param agent
	 * @param action
	 */
	public void doBonus(AgentKind agentKind, ItemType itemType);
}
