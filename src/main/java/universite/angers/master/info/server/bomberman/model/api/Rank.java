package universite.angers.master.info.server.bomberman.model.api;

/**
 * Enum qui permet de classer le joueur dans le jeu
 * Source : https://fireteam.fr/guides/grades-rocket-league/
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum Rank {

	UNRANKED("Pas de rang"),
	BRONZE("Bronze"),
	SILVER("Argent"),
	GOLD("Or"),
	PLATINIUM("Platine"),
	DIAMOND("Diamant"),
	CHAMPION("Champion");
	
	private String name;
	
	private Rank(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
