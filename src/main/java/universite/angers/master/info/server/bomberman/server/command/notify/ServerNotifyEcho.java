package universite.angers.master.info.server.bomberman.server.command.notify;

import java.util.Date;

import org.apache.log4j.Logger;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet d'afficher une notification inconnu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyEcho implements Commandable<Player> {
	
	private static final Logger LOG = Logger.getLogger(ServerNotifyEcho.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify default");
		
		Player player = new Player();
		
		player.getMessages().add("ECHO " + new Date());
		
		player.setCommand(Subject.NOTIFY_ECHO.getName());
		return player;
	}
}
