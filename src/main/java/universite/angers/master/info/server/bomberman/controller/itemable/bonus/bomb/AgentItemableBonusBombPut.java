package universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomb;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Augmente ou diminue le nombre de pose simultané de bombe à chaque tour
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusBombPut extends AgentItemableBonusBomb {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombPut() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.BOMB);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case BOMB_DOWN:
				return true;
			case BOMB_UP:
				return true;
			case BOMB_FULL_DOWN:
				return true;
			case BOMB_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.BOMB);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case BOMB_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case BOMB_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case BOMB_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case BOMB_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}

}
