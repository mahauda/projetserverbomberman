package universite.angers.master.info.server.bomberman.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.controller.checkable.CheckRuleMoveAgentEnnemyRandomly;
import universite.angers.master.info.server.bomberman.controller.checkable.RuleType;
import universite.angers.master.info.server.bomberman.ia.search.FactorySearch;
import universite.angers.master.info.server.bomberman.ia.search.Node;
import universite.angers.master.info.server.bomberman.ia.search.explorable.Explorable;
import universite.angers.master.info.server.bomberman.ia.search.explorable.ExplorationA;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.Heuristic;
import universite.angers.master.info.server.bomberman.ia.solve.heuristic.HeuristicBombermanManhattan;
import universite.angers.master.info.server.bomberman.model.Game;
import universite.angers.master.info.server.bomberman.model.bomberman.FactoryBombermanGame;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.server.bomberman.model.bomberman.map.MapAdapter;

/**
 * Controlleur qui permet d'assurer les contrôles du jeu quand des actions ont
 * été effectuées par l’utilisateur dans la Vue (par exemple lorsque
 * l’utilisateur clique sur un bouton)
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0 Created on : 22/12/2019
 *
 */
public class ControllerBombermanGame extends Controller<MapAdapter> {

	private static final Logger LOG = Logger.getLogger(ControllerBombermanGame.class);

	public ControllerBombermanGame(Game<MapAdapter> game) {
		super(game);
	}

	@Override
	public void init() {
		LOG.debug("Init game");
		//Il faut réinstancier le game à cause de la vue qui a du mal à se réinitialiser.
		//Comme les vues sont enregistrés en tant que observer dans le game, les références des vues ne sont pas totalement
		//supprimés lorsqu'on fait dispose() dans les vues
		//En supprimant le game, toutes les références enregistrés des vues sont bel et bien supprimés par le garbage collector.
		this.game = FactoryBombermanGame.getBombermanGameForPlayer(1000, 1000);
		this.game.init();
	}

	@Override
	public void start() {
		LOG.debug("Start game");
	}
	
	@Override
	public void step() {
		this.game.step(false);
	}

	@Override
	public void run() {
		this.game.launch(false);
	}

	@Override
	public void stop() {
		this.game.stop();
	}

	@Override
	public void setTime(long time) {
		this.game.setTime(time * 1000);
	}
	
	@Override
	public void open() {
		this.close();
		
		//On charge la map
		this.init();
		
		//On charge la vue
		this.start();
		
		//On charge le jeu
		this.run();
	}
	
	@Override
	public void close() {
		this.game.close();
	}
	
	/**
	 * Méthode qui permet de trouver une solution pour résoudre le niveau
	 */
	public void startSoluce() {
		// On enleve la règle de déplacement aléatoire des ennemis dans la résolution
		this.game.getRules().remove(RuleType.ENNEMY_MOVE_RANDOMLY);

		// On sauvegarde l'instance unique
		Map singleton = MapAdapter.getInstance();

		// On l'utilise dans l'ordre la stratégie suivante
		// 1 = haut
		// 2 = bas
		// 3 = gauche
		// 4 = droite
		// 5 = poser une bombe
		List<Integer> actions = new ArrayList<>();
		actions.add(1);
		actions.add(2);
		actions.add(3);
		actions.add(4);
		actions.add(5);

		// On utilise l'alorithme A* pour résoudre le probleme
		Explorable<MapAdapter> frontier = new ExplorationA<>();

		// On l'utilise l'heuristique de Manhanttan
		Heuristic<MapAdapter> heuristic = new HeuristicBombermanManhattan();

		Node<MapAdapter> find = FactorySearch.getSoluceBomberman(this.game.getMaxturn(), this.game.getTime(),
				Map.getInstance().getFilename(), actions, frontier, heuristic);

		// On récupère tous les noeuds pour résoudre le problème si la solution a été
		// trouvé
		// Si toutefois la solution n'a pas été trouvé dans le cas le joueur ne peut pas
		// réussir à le résoudre
		// Il faut donc afficher un message d'alerte
		LinkedList<Node<MapAdapter>> soluces = new LinkedList<>();
		if (find != null)
			soluces = find.getAllNodes();

		// On réinjecte le singleton
		MapAdapter.setInstance(singleton);

		// On remet la règle de déplacement aléatoire des ennemis pour le jeu
		this.game.getRules().put(RuleType.ENNEMY_MOVE_RANDOMLY, new CheckRuleMoveAgentEnnemyRandomly());

		// On donne la liste des solutions au jeu
		this.game.setSoluces(soluces);
	}
}