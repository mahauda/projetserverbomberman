package universite.angers.master.info.server.bomberman.controller.actionnable;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Attaquer un autre agent. En l'occurence l'actuel bomberman manipulé dans le plateau
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentActionnableAttack extends AgentActionnable {

	private static final long serialVersionUID = 1L;
	
	@Override
	public boolean isLegalAction(Agent agent, AgentAction action) {
		//Il peut attaquer s'il se trouve sur la même coordonnée
		
		//On récupère les coordonnées de l'ennemi
		int ennemyX = agent.getX();
		int ennemyY = agent.getY();
		
		//On récupère les coordonnées du bomberman
		int kindX = Map.getInstance().getCurrentAgentKind().getX();
		int kindY = Map.getInstance().getCurrentAgentKind().getY();
		
		//Si l'ennemi est sur le bomberman
		return action == AgentAction.ATTACK && ennemyX == kindX && ennemyY == kindY;
	}

	@Override
	public void doAction(Agent agent, AgentAction action) {
		//On récupère le dégat de l'ennemi
		CapacityItem attack = agent.getCapacityItem(ItemType.ATTACK);
		if(attack == null) return;
		
		int degat = attack.getCapacityActualItem();
		
		//On applique le dégat sur l'agent bomberman
		CapacityItem life = Map.getInstance().getCurrentAgentKind().getCapacityItem(ItemType.LIFE);
		if(life == null) return;
		
		int saveStep = life.getStep();
		life.setStep(degat);
		life.decrementCapacityActualItem();
		life.setStep(saveStep);
	}
}
