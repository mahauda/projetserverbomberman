package universite.angers.master.info.server.bomberman.controller.itemable.bonus.life;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Augmente ou diminue le nombre de vie
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusLifeHeart extends AgentItemableBonusLife {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusLifeHeart() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.LIFE);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case LIFE_DOWN:
				return true;
			case LIFE_UP:
				return true;
			case LIFE_FULL_DOWN:
				return true;
			case LIFE_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.LIFE);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case LIFE_DOWN:
				//le bomberman peut mourrir
				capacityItem.decrementCapacityActualItem();
				break;
			case LIFE_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case LIFE_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				//le bomberman meurt obigatoirement
				break;
			case LIFE_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}

}
