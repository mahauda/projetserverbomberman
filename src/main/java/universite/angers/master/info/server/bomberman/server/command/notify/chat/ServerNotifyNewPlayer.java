package universite.angers.master.info.server.bomberman.server.command.notify.chat;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet d'informer l'arrivé d'un nouveau joueur dans la partie
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyNewPlayer implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyNewPlayer.class);

	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify new player");
		Player playermessage = new Player();
		

		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if (arg == null) {
			player = new Player();
		} else {
			player = (Player) arg;
		}
		
		// On indique au autres joueur lavenue d'un autre joueur
		playermessage.getMessages().add("[Serveur] : Le joueur " +  player.getLogin() + " s'est connecté");
		LOG.debug("Le joueur " + player.getLogin() + " s'est connecté");
		
		playermessage.setCommand("NOTIFY_NEW_PLAYER");
		LOG.debug("Player : " + playermessage);
		
		return playermessage;
	}
}
