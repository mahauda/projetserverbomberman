package universite.angers.master.info.server.bomberman.controller.checkable;

import java.io.Serializable;

/**
 * Regle commun au jeu Bomberman
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class CheckRule implements Checkable, Serializable {
	
	private static final long serialVersionUID = 1L;

	public CheckRule() {
		
	}
}
