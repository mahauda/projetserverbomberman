package universite.angers.master.info.server.bomberman.server.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.api.User;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;
import universite.angers.master.info.server.bomberman.server.api.UserAPI;

/**
 * Classe qui permet d'envoyer une requete au serveur pour s'identifier
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestLogin implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerRequestLogin.class);

	private Player player;

	@Override
	public boolean send(Player player) {
		LOG.debug("Request login player : " + player);

		this.player = new Player();
		this.player.setLogin(player.getLogin());
		this.player.setPassword(player.getPassword());

		// Vérifier que l'identifiant du joueur est existant dans la
		// BDD de JEE et l'enregistrer

		// Interroger l'API JEE pour vérifier si le joueur est enregistré à partir des
		// identifiants contenu dans player

		User user = UserAPI.getInstance().read("pseudo", player.getLogin());
		LOG.debug("Request login user : " + user);

		// Si l'utilisateur est null dans ce cas joueur non enregistré et record false

		if (user == null) {
			this.player.getMessages()
					.add("Pseudo " + player.getLogin() + " non reconnu. Vous devez vous enregistrer !");
			this.player.setRecord(false);

			return true;
		}

		// Si mot de passe utilisateur est incorrect dans ce cas mot de passe incorrect
		// et record false

		if (!user.getPassword().equals(player.getPassword())) {
			LOG.debug("    user password    : " + user.getPassword());
			LOG.debug("   player password   :" + player.getPassword());
			LOG.debug("Mot de passe incorrect !");

			this.player.getMessages().add("Mot de passe incorrect !");
			this.player.setRecord(false);

			return true;
		}

		// Si tout est ok, alors il faut enregistrer le joueur dans le réseau
		
		// On enregistre le player dans la map
		LOG.debug("Bienvenue !" + player.getLogin());

		// Si c'est le premier joueur dans ce cas on démarre le jeu
		if (ServerBombermanGame.getInstance().getPlayers().isEmpty()) {
			ServerBombermanGame.getInstance().getControllerBombermanGame().open();
			// Si cest le cas le joueur qui vient de rentrer sera le premier a jouer
			
			player.setPlayTurn(true);
			ServerBombermanGame.getInstance().notifyObserverOnlyOnePlayer(player, "NOTIFY_YOUR_TURN", player);
		} else {
			player.setPlayTurn(false);

			ServerBombermanGame.getInstance().notifyAllObserversWithoutPlayer(player,"NOTIFY_NEW_PLAYER",player);
		}
		// Sinon pour les autres joueurs qui arrive nul besoin de démarrer le jeu à
		// nouveau
		// Ils arrivent en cours de la partie

		// On ajoute le joueur
		ServerBombermanGame.getInstance().addPlayer(player);
		
		//On récupère la nouvelle map à afficher coté client
		String filename = Map.getInstance().getFilename();
		LOG.debug("File name : " + filename);
		
		this.player.setPathMap(filename);

		// on souhaite la bienvenue au joueur
		this.player.getMessages().add("[Serveur] : Bienvenue " + player.getLogin());

		this.player.setRecord(true);
		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_LOGIN");
		return this.player;
	}
}