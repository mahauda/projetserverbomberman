package universite.angers.master.info.server.bomberman.ia.solve.heuristic;

import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;

/**
 * Calcul de la distance Euclidienne
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class HeuristicBombermanEuclidienne extends HeuristicBomberman {

	public HeuristicBombermanEuclidienne() {

	}

	@Override
	public double calculDistance(Agent a, Agent b) {
		return Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
	}

	@Override
	public String toString() {
		return "HeuristicBombermanEuclidienne [h=" + h + ", g=" + g + ", f=" + f + ", getH()=" + getH() + ", getG()="
				+ getG() + ", getF()=" + getF() + "]";
	}
}
